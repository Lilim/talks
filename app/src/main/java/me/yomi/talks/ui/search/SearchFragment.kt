package me.yomi.talks.ui.search

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import me.yomi.talks.R
import me.yomi.talks.databinding.FragmentSearchBinding
import me.yomi.talks.ui.ActivityOperationsListener
import me.yomi.talks.ui.FavoritesChangedListener
import me.yomi.talks.ui.detail.DetailActivity
import me.yomi.talks.ui.detail.DetailFragment
import me.yomi.talks.ui.detail.OpenDetailFragmentListener
import me.yomi.talks.ui.room.EventAdapter

class SearchFragment : Fragment(),
    OpenDetailFragmentListener, FavoritesChangedListener, ActivityOperationsListener {
    private lateinit var searchModel: SearchViewModel
    private lateinit var binding: FragmentSearchBinding
    private lateinit var adapter: EventAdapter
    private lateinit var searchView: SearchView
    private lateinit var activityOperationsListener: ActivityOperationsListener

    private val mHandler: Handler = Handler()
    private lateinit var mRunnable: Runnable

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        searchModel = ViewModelProviders.of(this)[SearchViewModel::class.java]

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = EventAdapter(context!!, true, this, "search")
        adapter.setFavoritesChangedListener(this)
        binding.recyclerview.layoutManager = LinearLayoutManager(context)
        binding.recyclerview.adapter = adapter

        searchModel.resultList.observe(this, Observer {
            adapter.setEventList(it)
        })
    }

    fun onSearchSubmitted(query: String) {
        if (::mRunnable.isInitialized) {
            mHandler.removeCallbacks(mRunnable)
        }

        mRunnable = Runnable {
            searchModel.searchString = query
            searchModel.searchOpened = query.isNotEmpty()
            searchModel.doSearch(query)
        }

        mHandler.postDelayed(mRunnable, 500)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search, menu)

        val menuSearchView = menu.findItem(R.id.action_search)
        searchView = menuSearchView.actionView as SearchView
        if (searchModel.searchOpened && searchModel.searchString.isNotEmpty()) {
            menuSearchView.expandActionView()
            searchView.isIconified = !searchModel.searchOpened
            searchView.setQuery(searchModel.searchString, false)
            onSearchSubmitted(searchModel.searchString)
        }
        menuSearchView.expandActionView()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(newText: String): Boolean {
//                if (isVisible) {
                onSearchSubmitted(newText)
//                }
                return true
            }

            override fun onQueryTextSubmit(query: String): Boolean {
//                if (isVisible) {
                onSearchSubmitted(query)
//                }
                return true
            }
        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun openDetailFragment(detailFragment: DetailFragment, tag: String) {
    }

    override fun openDetailActivity(guid: String, tag: String) {
        val intent = Intent(context, DetailActivity::class.java)
        intent.putExtra("guid", guid)
        activity?.startActivity(intent)
        searchModel.subViewOpened = true
    }

    override fun onFavoritesChanged(guid: String, insert: Boolean, room: String, origin: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun closeSubview() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openedSubview(name: String) {
        searchView.visibility = View.GONE
        searchModel.subViewOpened = true
    }

    fun setBackButtonListener(activityOperationsListener: ActivityOperationsListener) {
        this.activityOperationsListener = activityOperationsListener
    }
}