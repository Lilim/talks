package me.yomi.talks.ui.settings

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_color_setting.view.*
import me.yomi.talks.core.database.entities.ColorEntity
import me.yomi.talks.ui.Utils

class ColorSettingViewHolder(itemView: View)  : RecyclerView.ViewHolder(itemView) {

    fun bindItem(item: ColorEntity, context: Context) {
        itemView.room_name.text = item.room
        val color = Utils.getColorByName(item.backgroundColor)
        println("## color " + item.backgroundColor)
//        itemView.color_preview.setBackgroundColor(color.first)
    }

}