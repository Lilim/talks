package me.yomi.talks.ui.room

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.event_item.view.*
import me.yomi.talks.R
import me.yomi.talks.core.database.entities.EventEntity
import me.yomi.talks.ui.FavoritesChangedListener
import me.yomi.talks.ui.detail.DetailFragment
import me.yomi.talks.ui.detail.OpenDetailFragmentListener

class EventAdapter(
    val context: Context,
    private val insideSearch: Boolean,
    private val openDetailFragmentListener: OpenDetailFragmentListener,
    private val origin: String
) : RecyclerView.Adapter<EventViewHolder>() {
    private var eventList: List<EventEntity>? = null
    private lateinit var favoritesChangedListener: FavoritesChangedListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val view = if (insideSearch) {
            LayoutInflater.from(parent.context).inflate(R.layout.event_item_search, parent, false)
        } else {
            if (viewType == 1) {
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.event_header_item, parent, false)
            } else {
                LayoutInflater.from(parent.context).inflate(R.layout.event_item, parent, false)
            }
        }
        return EventViewHolder(view)
    }

    fun setEventList(newEventList: List<EventEntity>) {
        this.eventList = newEventList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return eventList?.size ?: 0
    }

    override fun getItemViewType(position: Int): Int {
        return if (eventList?.get(position)?.guid.equals("header")) {
            1
        } else {
            0
        }
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        eventList?.get(position)?.let { item -> holder.bindItem(item, context, origin) }

        if (!eventList?.get(position)?.guid.equals("header")) {
            holder.itemView.itemBackground.setOnClickListener {
                val guid = eventList?.get(position)!!.guid
                if (insideSearch) {
                    openDetailFragmentListener.openDetailActivity(guid, "DETAIL")
                } else {
                    val detailFragment = DetailFragment.newInstance(guid, true)
                    detailFragment.setFavoritesChangedListener(favoritesChangedListener)
                    openDetailFragmentListener.openDetailFragment(detailFragment, "DETAIL")
                }
            }

            holder.itemView.favorite.setOnClickListener {
                if (eventList!![position].isFavorite) {
                    holder.itemView.favorite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_star_border_24px))
                    eventList!![position].isFavorite = false
                } else {
                    holder.itemView.favorite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_star_24px))
                    eventList!![position].isFavorite = true
                }
                favoritesChangedListener.onFavoritesChanged(
                    eventList!![position].guid,
                    eventList!![position].isFavorite,
                    eventList!![position].room,
                    origin
                )
            }
        }
    }

    fun updateItem(guid: String, isFavorite: Boolean) {
        val pos = eventList?.indexOfFirst {
            it.guid == guid
        }

        if (pos != null && pos > -1) {
            eventList!![pos].isFavorite = isFavorite
            notifyItemChanged(pos)
        }
    }

    fun setFavoritesChangedListener(favoritesChangedListener: FavoritesChangedListener) {
        this.favoritesChangedListener = favoritesChangedListener
    }
}