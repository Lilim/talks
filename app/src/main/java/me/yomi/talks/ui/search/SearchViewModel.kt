package me.yomi.talks.ui.search

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.runBlocking
import me.yomi.talks.core.database.DatabaseRepository
import me.yomi.talks.core.database.entities.EventEntity

class SearchViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: DatabaseRepository = DatabaseRepository(application, viewModelScope)
    val resultList = MutableLiveData<List<EventEntity>>()
    var subViewOpened: Boolean = false
    var searchOpened: Boolean = false
    var searchString: String = ""


    fun doSearch(query: String) = runBlocking {
        val list = repository.doSearch(query)
        resultList.value = list
    }
}