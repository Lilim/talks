package me.yomi.talks.ui.schedule

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.runBlocking
import me.yomi.talks.core.database.DatabaseRepository
import me.yomi.talks.core.database.entities.ColorEntity
import me.yomi.talks.core.network.models.EventItemSchedule
import me.yomi.talks.ui.Utils
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ScheduleViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: DatabaseRepository = DatabaseRepository(application, viewModelScope)
    private val dateFormatHourMinute = SimpleDateFormat("HH:mm", Locale.getDefault())
    private lateinit var colorList: List<ColorEntity>
    val eventList = MutableLiveData<List<EventItemSchedule>>()
    var canOverlap: Boolean = true
    var room: String = ""
    var currentDate: String = ""
    var timeHeight: Int = 0
    var timeWidth: Int = 0
    var minuteHeight: Double = 0.0
    val hourFirstRow: Int = 9
    val scheduleStartTime: String = "09:00"
    val hoursOfCongressDay: Int = 18
    var defaultMargin: Int = 0
    var dividerMargin: Int = 0

    fun formatDateToHHmm(date: Date): String {
        return dateFormatHourMinute.format(date)
    }

    fun formatStringToHHmm(dateString: String): Date {
        return dateFormatHourMinute.parse(dateString)
    }

    fun getEvents(room: String, day: String) = runBlocking {
        val list = if (room == "Favorites") {
            repository.getFavoritesByDay(day)
        } else {
            repository.getEventsByRoomAndDay(room, day)
        }
        val tempList = ArrayList<EventItemSchedule>()
        for (item in list) {
            tempList.add(EventItemSchedule(item, false, -1))
        }
        eventList.postValue(tempList)
    }

    fun getColors() = runBlocking {
        val list = repository.getColors()
        colorList = list
    }

    fun getColorsForRoom(room: String): Pair<Int, Int> {
        getColors()
        val color = colorList.find {
            it.room == room
        }
        return Utils.getColorByName(color?.backgroundColor)
    }
}