package me.yomi.talks.ui.room

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import me.yomi.talks.R
import me.yomi.talks.core.Constants
import me.yomi.talks.databinding.RoomFragmentBinding
import me.yomi.talks.ui.FavoritesChangedListener
import me.yomi.talks.ui.custom.ViewPagerFragment
import me.yomi.talks.ui.detail.DetailFragment
import me.yomi.talks.ui.detail.OpenDetailFragmentListener

class RoomFragment : ViewPagerFragment(),
    OpenDetailFragmentListener, FavoritesChangedListener {
    private lateinit var binding: RoomFragmentBinding
    private lateinit var adapter: EventAdapter
    private lateinit var roomViewModel: RoomViewModel
    private lateinit var favoritesChangedListener: FavoritesChangedListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.room_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        roomViewModel = ViewModelProviders.of(this)[RoomViewModel::class.java]

        setObserver()

        val args = arguments
        if (args != null) {
            val name = args.getString("name", "")
            val currentDate = args.getString("date", "")
            if (name.isNotEmpty()) {
                roomViewModel.roomName = name
                super.setRoomName(name)
                roomViewModel.currentDate = currentDate
            }
        }

        adapter = EventAdapter(context!!, false, this, roomViewModel.roomName)
        adapter.setFavoritesChangedListener(this)
        binding.recyclerview.layoutManager = LinearLayoutManager(context)
        binding.recyclerview.adapter = adapter

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        getData()
    }

    private fun getData() {
        when (roomViewModel.roomName) {
            Constants.FAVORITES -> roomViewModel.getFavorites()
            Constants.ALL -> roomViewModel.getAllEventsForTheDay()
            else -> roomViewModel.getEvents(roomViewModel.roomName, roomViewModel.currentDate)
        }
    }

    companion object {
        fun newInstance(name: String, date: String): Fragment {
            val args = Bundle()
            args.putString("name", name)
            args.putString("date", date)

            val fragment = RoomFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private fun setObserver() {
        roomViewModel.eventList.observe(this, Observer {
            adapter.setEventList(it)
        })
    }

    override fun openDetailFragment(detailFragment: DetailFragment, tag: String) {
        val fm = activity?.supportFragmentManager
        fm?.beginTransaction()
            ?.add(R.id.fragmentContainer, detailFragment, tag)
            ?.addToBackStack(tag)
            ?.commit()
    }

    override fun openDetailActivity(guid: String, tag: String) {
    }

    override fun updateFavoriteInView(guid: String, insert: Boolean) {
        adapter.updateItem(guid, insert)
    }

    fun setFavoritesChangedListener(favoritesChangedListener: FavoritesChangedListener) {
        this.favoritesChangedListener = favoritesChangedListener
    }

    override fun onFavoritesChanged(guid: String, insert: Boolean, room: String, origin: String) {
        updateFavoriteInView(guid, insert)
        favoritesChangedListener.onFavoritesChanged(guid, insert, room, origin)
    }
}