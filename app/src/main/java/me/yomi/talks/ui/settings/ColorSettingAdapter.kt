package me.yomi.talks.ui.settings

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.yomi.talks.R
import me.yomi.talks.core.database.entities.ColorEntity

class ColorSettingAdapter(val context: Context) : RecyclerView.Adapter<ColorSettingViewHolder>() {

    private var colorList: List<ColorEntity>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorSettingViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_color_setting, parent,false)
        return ColorSettingViewHolder(view)
    }

    override fun getItemCount(): Int {
        return colorList?.size ?: 0
    }

    fun setColorList(newColorList: List<ColorEntity>) {
        this.colorList = newColorList
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ColorSettingViewHolder, position: Int) {
        colorList?.get(position)?.let { item -> holder.bindItem(item, context) }
    }
}