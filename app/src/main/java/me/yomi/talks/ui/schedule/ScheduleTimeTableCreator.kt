package me.yomi.talks.ui.schedule

import android.content.Context
import android.graphics.Color
import android.util.TypedValue
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import me.yomi.talks.R
import java.util.*

class ScheduleTimeTableCreator(
    private val context: Context,
    private val scheduleViewModel: ScheduleViewModel
) {
    private val calendarSchedule: Calendar = Calendar.getInstance()
    private val bottomDividerLp =
        RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 1)
    private val insideDividerLp =
        RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 1)
    private lateinit var timeContainerLp: LinearLayout.LayoutParams
    private lateinit var defaultLp: LinearLayout.LayoutParams
    private val timeLp = RelativeLayout.LayoutParams(
        RelativeLayout.LayoutParams.WRAP_CONTENT,
        RelativeLayout.LayoutParams.WRAP_CONTENT
    )

    init {
        initLayouts()
    }

    private fun initLayouts() {
        // Date is only used for displaying time in the rows
        val scheduleDate = scheduleViewModel.formatStringToHHmm(scheduleViewModel.scheduleStartTime)
        calendarSchedule.time = scheduleDate

        bottomDividerLp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        insideDividerLp.addRule(RelativeLayout.CENTER_IN_PARENT)
        val negativeMargin = context.resources.getDimensionPixelSize(R.dimen.negativeMargin)
        timeContainerLp =
            LinearLayout.LayoutParams(
                scheduleViewModel.timeWidth,
                scheduleViewModel.timeHeight + negativeMargin
            )
        timeLp.addRule(RelativeLayout.CENTER_HORIZONTAL)
        timeContainerLp.topMargin = negativeMargin * -1

        defaultLp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            scheduleViewModel.timeHeight
        )
    }

    fun setUpTime(timesContainer: LinearLayout) {

        val timeContainerView = RelativeLayout(context)
        timeContainerView.layoutParams = timeContainerLp

        // Set up time view
        val timeView = TextView(context)
        timeView.layoutParams = timeLp
        timeView.setTextColor(Color.BLACK)
        timeView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10f)
        timeView.text = scheduleViewModel.formatDateToHHmm(calendarSchedule.time)

        timeContainerView.addView(timeView)
        timesContainer.addView(timeContainerView)

        // Add hour for next row
        calendarSchedule.add(Calendar.HOUR, 1)
    }

    fun setUpRow(backgroundRowsContainer: LinearLayout) {
        // Create row view
        val rowView = RelativeLayout(context)
        rowView.layoutParams = defaultLp

        val insideDivider = View(context)
        insideDivider.layoutParams = insideDividerLp
        insideDivider.setBackgroundColor(context.resources.getColor(R.color.grey))
        rowView.addView(insideDivider)

        val bottomDivider = View(context)
        bottomDivider.layoutParams = bottomDividerLp
        bottomDivider.setBackgroundColor(context.resources.getColor(R.color.grey))
        rowView.addView(bottomDivider)
        backgroundRowsContainer.addView(rowView)
    }
}