package me.yomi.talks.ui.custom

import android.view.View
import android.widget.AdapterView
import me.yomi.talks.MainActivity

class SpinnerSelect(
    private val mainActivity: MainActivity,
    private val dateList: List<String>
) : AdapterView.OnItemSelectedListener {

    override fun onNothingSelected(parent: AdapterView<*>?) {}

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        mainActivity.updateViewPager(dateList[position])
    }
}