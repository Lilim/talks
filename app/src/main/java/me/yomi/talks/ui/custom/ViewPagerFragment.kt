package me.yomi.talks.ui.custom

import androidx.fragment.app.Fragment

open class ViewPagerFragment : Fragment() {
    private var roomName: String = ""

    fun setRoomName(name: String) {
        roomName = name
    }

    fun getRoomName(): String {
        return roomName
    }

    open fun updateFavoriteInView(guid: String, insert: Boolean) {

    }
}