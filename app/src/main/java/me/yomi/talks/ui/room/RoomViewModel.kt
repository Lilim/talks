package me.yomi.talks.ui.room

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import me.yomi.talks.core.database.DatabaseRepository
import me.yomi.talks.core.database.entities.EventEntity
import java.util.*
import kotlin.collections.ArrayList

class RoomViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: DatabaseRepository = DatabaseRepository(application, viewModelScope)
    val eventList = MutableLiveData<List<EventEntity>>()
    var roomName = ""
    var currentDate = ""

    fun getEvents(room: String, day: String) = viewModelScope.launch {
        val list = repository.getEventsByRoomAndDay(room, day)
        eventList.postValue(list)
    }

    fun getFavorites() = viewModelScope.launch {
        val list = repository.getFavoritesByDay(currentDate)
        eventList.postValue(list)
    }

    fun getAllEventsForTheDay() = viewModelScope.launch {
        val list = repository.getEventsByDay(currentDate)
        val tempList = ArrayList<EventEntity>()
        var timeHeader = ""
        val calendar = Calendar.getInstance()
        for (event in list) {
            calendar.time = Date(event.timestampStart)
            val checkTime = "${String.format(
                "%02d",
                calendar.get(Calendar.HOUR_OF_DAY)
            )} : ${String.format("%02d", calendar.get(Calendar.MINUTE))}"
            if (!timeHeader.equals(checkTime)) {
                tempList.add(
                    EventEntity(
                        "header", 0, "", "", "", "",
                        checkTime, "", "", "", "", "", "", "", null, "", 0, 0, "", false
                    )
                )
            }
            tempList.add(event)
            timeHeader = "${String.format(
                "%02d",
                calendar.get(Calendar.HOUR_OF_DAY)
            )} : ${String.format("%02d", calendar.get(Calendar.MINUTE))}"
        }

        eventList.value = tempList
    }
}