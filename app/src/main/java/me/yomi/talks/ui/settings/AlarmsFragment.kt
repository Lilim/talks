package me.yomi.talks.ui.settings

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import me.yomi.talks.R
import me.yomi.talks.databinding.FragmentAlarmsBinding


class AlarmsFragment : Fragment() {
    private lateinit var binding: FragmentAlarmsBinding
    private lateinit var alarmsViewModel: AlarmsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_alarms, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        alarmsViewModel = ViewModelProviders.of(this)[AlarmsViewModel::class.java]

        alarmsViewModel.alarmsList.observe(this, Observer {

        })

        alarmsViewModel.getAlarms()

        return binding.root
    }

    fun cancelAlarmIfExists(
        mContext: Context,
        requestCode: Int,
        intent: Intent?
    ) {
        try {
            val pendingIntent =
                PendingIntent.getBroadcast(mContext, requestCode, intent, 0)
            val am = mContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
            am.cancel(pendingIntent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}