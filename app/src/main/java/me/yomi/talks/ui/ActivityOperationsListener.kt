package me.yomi.talks.ui

interface ActivityOperationsListener {
    fun closeSubview()
    fun openedSubview(name: String)
}