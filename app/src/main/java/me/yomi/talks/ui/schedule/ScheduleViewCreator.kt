package me.yomi.talks.ui.schedule

import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import me.yomi.talks.R
import me.yomi.talks.core.network.models.EventItemSchedule
import me.yomi.talks.ui.FavoritesChangedListener
import me.yomi.talks.ui.detail.DetailFragment
import me.yomi.talks.ui.detail.OpenDetailFragmentListener
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.layoutInflater
import java.util.*

class ScheduleViewCreator(
    private val scheduleViewModel: ScheduleViewModel
) {

    /**
     * Check if there are one or more events starting during another event is
     * ongoing
     * Save event id to get width and margin later
     */
    fun preCalculateLeftMargin(list: List<EventItemSchedule>) {
        for (event in list) {
            for (checkEvent in list) {
                if (event.eventItem.guid == checkEvent.eventItem.guid) {
                    break
                } else {
                    if (event.eventItem.timestampStart < checkEvent.eventItem.timestampEnd) {
                        event.isIndentedByEventId = checkEvent.eventItem.id.toInt()
                    }
                }
            }
        }
    }

    /**
     * If there are parallel events move views to display them next to each other
     */
    fun postCalculateMargin(list: List<EventItemSchedule>, eventContainer: RelativeLayout) {
        for (event in list) {
            if (event.isIndentedByEventId > 0) {
                val view = eventContainer.findViewById<ConstraintLayout>(event.isIndentedByEventId)
                if (view != null) {
                    setObserverForLayoutChange(view, event, eventContainer)
                } else {
                    println("## view not found")
                }
            }
        }
    }

    /**
     * Wait till view is drawn to get width and margin for setting margin of new event view
     * Also add divider margin
     */
    private fun setObserverForLayoutChange(
        view: ConstraintLayout,
        event: EventItemSchedule,
        eventContainer: RelativeLayout
    ) {
        view.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (view.width > 0) {
                    view.viewTreeObserver.removeOnGlobalLayoutListener(this)

                    val indentedEventView =
                        eventContainer.findViewById<ConstraintLayout>(event.eventItem.id.toInt())
                    if (indentedEventView != null) {
                        val lp =
                            indentedEventView.layoutParams as RelativeLayout.LayoutParams

                        val lp2 = view.layoutParams as ViewGroup.MarginLayoutParams
                        val additionalMargin = lp2.marginStart
                        lp.marginStart =
                            view.width + additionalMargin + scheduleViewModel.dividerMargin
                        indentedEventView.layoutParams = lp
                    }
                }
            }
        })
    }

    fun createEvents(
        list: List<EventItemSchedule>,
        eventContainer: RelativeLayout,
        context: Context,
        openDetailFragmentListener: OpenDetailFragmentListener,
        favoritesChangedListener: FavoritesChangedListener
    ) {
        for (event in list) {
            val view = createEventView(
                event,
                context,
                openDetailFragmentListener,
                favoritesChangedListener
            )
            eventContainer.addView(view)
        }
    }

    private fun createEventView(
        event: EventItemSchedule,
        context: Context,
        openDetailFragmentListener: OpenDetailFragmentListener,
        favoritesChangedListener: FavoritesChangedListener
    ): ConstraintLayout {
        val displayData = formatDates(
            event.eventItem.timestampStart,
            event.eventItem.timestampEnd,
            event.eventItem.duration
        )

        val view =
            context.layoutInflater.inflate(R.layout.event_day_item, null, false) as ConstraintLayout
        view.id = event.eventItem.id.toInt()
        val title = view.findViewById<TextView>(R.id.title)
        val time = view.findViewById<TextView>(R.id.time)
        val track = view.findViewById<TextView>(R.id.track)

        val newLayoutParams =
            LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                displayData.eventContainerHeight - 4
            )
        newLayoutParams.marginEnd = context.resources.getDimensionPixelSize(R.dimen.padding5dp)
        newLayoutParams.topMargin = displayData.topViewMargin + 4
        view.layoutParams = newLayoutParams

        title.text = event.eventItem.title
        time.text = "${displayData.displayTime} ${event.eventItem.room}"
        track.text = event.eventItem.track + " [" + event.eventItem.language + "]"

        // TODO Maybe style?
        val colors = scheduleViewModel.getColorsForRoom(event.eventItem.room)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            view.backgroundTintList =
                ColorStateList.valueOf(context.resources.getColor(colors.first))
        } else {
            view.backgroundColor = colors.first
        }

        if (colors.second != R.color.white) {
            setTextColor(title, colors.second)
            setTextColor(time, colors.second)
            setTextColor(track, colors.second)
        }

        // Hide track view in short events
        if (displayData.smallHeight) {
            track.visibility = View.GONE
        }

        view.setOnClickListener {
            val detailFragment = DetailFragment.newInstance(event.eventItem.guid, true)
            detailFragment.setFavoritesChangedListener(favoritesChangedListener)
            openDetailFragmentListener.openDetailFragment(detailFragment, "DETAILS")
        }

        view.setOnLongClickListener {
            if (event.eventItem.isFavorite) {
                event.eventItem.isFavorite = false
                favoritesChangedListener.onFavoritesChanged(
                    event.eventItem.guid,
                    false,
                    event.eventItem.room,
                    scheduleViewModel.room
                )
            } else {
                event.eventItem.isFavorite = true
                favoritesChangedListener.onFavoritesChanged(
                    event.eventItem.guid,
                    true,
                    event.eventItem.room,
                    scheduleViewModel.room
                )
            }
            true
        }
        return view
    }

    private fun setTextColor(view: TextView, color: Int) {
        view.setTextColor(color)
    }

    private fun formatDates(startTimestamp: Long, endTimestamp: Long, duration: String): Display {

        // Set start time
        val calendarStart = Calendar.getInstance()
        calendarStart.timeInMillis = startTimestamp
        val start = String.format(
            "%02d:%02d",
            calendarStart.get(Calendar.HOUR_OF_DAY),
            calendarStart.get(Calendar.MINUTE)
        )

        // Calculate top margin
        // Depends on the hour of the first row
        // On day overlapping hours put the event to the end +24h
        var topViewMargin = scheduleViewModel.defaultMargin
        val minutesPixelSize = calendarStart.get(Calendar.MINUTE) * scheduleViewModel.minuteHeight
        if (calendarStart.get(Calendar.HOUR_OF_DAY) >= scheduleViewModel.hourFirstRow) {
            topViewMargin +=
                ((calendarStart.get(Calendar.HOUR_OF_DAY) - scheduleViewModel.hourFirstRow) * scheduleViewModel.timeHeight) + minutesPixelSize.toInt()
        } else if (calendarStart.get(Calendar.HOUR_OF_DAY) < scheduleViewModel.hourFirstRow) {
            topViewMargin +=
                ((calendarStart.get(Calendar.HOUR_OF_DAY) + (24 - scheduleViewModel.hourFirstRow)) * scheduleViewModel.timeHeight) + minutesPixelSize.toInt()
        }

        // Calculate event height
        val split = duration.split(":")
        val hours = split[0].toInt()
        val minutes = split[1].toInt()
        val eventContainerHeight =
            (hours * scheduleViewModel.timeHeight) + (minutes * scheduleViewModel.minuteHeight).toInt()

        // Format end time
        val calendarEnd = Calendar.getInstance()
        calendarEnd.timeInMillis = endTimestamp
        val end = String.format(
            "%02d:%02d",
            calendarEnd.get(Calendar.HOUR_OF_DAY),
            calendarEnd.get(Calendar.MINUTE)
        )
        val displayTime = "$start - $end"

        val smallHeight: Boolean = hours < 1 && minutes < 40

        return Display(topViewMargin, eventContainerHeight, displayTime, smallHeight)
    }

    class Display(
        val topViewMargin: Int,
        val eventContainerHeight: Int,
        val displayTime: String,
        val smallHeight: Boolean
    )
}