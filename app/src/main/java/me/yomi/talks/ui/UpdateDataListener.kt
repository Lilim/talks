package me.yomi.talks.ui

import me.yomi.talks.core.network.ResponseStatus

interface UpdateDataListener {
    fun dataFetched(responseStatus: ResponseStatus)
}