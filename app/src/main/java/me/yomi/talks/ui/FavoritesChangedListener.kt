package me.yomi.talks.ui

interface FavoritesChangedListener {
    fun onFavoritesChanged(guid: String, insert: Boolean, room: String, origin: String)
}