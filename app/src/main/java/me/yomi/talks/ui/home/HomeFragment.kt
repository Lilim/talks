package me.yomi.talks.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import me.yomi.talks.R
import me.yomi.talks.core.Constants
import me.yomi.talks.databinding.HomeFragmentBinding
import me.yomi.talks.ui.FavoritesChangedListener

class HomeFragment : Fragment(), FavoritesChangedListener {
    private lateinit var binding: HomeFragmentBinding
    private lateinit var pageAdapter: RoomsPagerAdapter
    private lateinit var homeViewModel: HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        homeViewModel = ViewModelProviders.of(this)[HomeViewModel::class.java]

        pageAdapter =
            RoomsPagerAdapter(childFragmentManager, this)
        binding.viewPager.adapter = pageAdapter

        binding.tabLayout.setupWithViewPager(binding.viewPager)

        homeViewModel.roomsList.observe(this, Observer {
            pageAdapter.setRooms(it)
            pageAdapter.setCurrentDate(homeViewModel.currentDate)
            homeViewModel.initialSetupDone = true
        })

        return binding.root
    }

    fun updateData(newDate: String, favChanged: Boolean) {
        if (newDate != homeViewModel.currentDate || favChanged) {
            homeViewModel.currentDate = newDate
            homeViewModel.getRooms()
        }
    }

    override fun onFavoritesChanged(guid: String, insert: Boolean, room: String, origin: String) {
        homeViewModel.changeFavoriteInDatabase(guid, insert)

        // If after favoriteslist update in changeFavoriteInDatabase size is 1 and insert is true
        // the viewpager should update to show favorites tab
        if (homeViewModel.favoritesList.value?.size == 1 && insert) {
            val pos = binding.viewPager.currentItem
            pageAdapter.addFavoriteFragment()
            pageAdapter.notifyDataSetChanged()
            binding.viewPager.currentItem = pos + 1
            return
        }

        // Same goes for if list is 0 and insert is false -> viewpager should update
        if (homeViewModel.favoritesList.value?.size == 0 && !insert) {
            val pos = binding.viewPager.currentItem
            pageAdapter.removeFavoriteFragment()
            pageAdapter.notifyDataSetChanged()
            binding.viewPager.currentItem = if (pos > 0) {
                pos - 1
            } else {
                binding.tabLayout.smoothScrollTo(0, 0)
                pos
            }
            return
        }

        if (origin == Constants.FAVORITES) {
            pageAdapter.updateFavoriteFragment(guid, insert)
            pageAdapter.updateFragmentByRoomName(guid, insert, room)
        } else {
            pageAdapter.updateFavoriteFragment(guid, insert)
        }
    }
}