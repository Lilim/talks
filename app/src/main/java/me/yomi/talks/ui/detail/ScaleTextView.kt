package me.yomi.talks.ui.detail

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.util.TypedValue
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.widget.TextView

class ScaleTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : TextView(context, attrs, defStyleAttr) {


    private var scaleListener = object : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        private var mScaleFactor = 1f


        override fun onScale(detector: ScaleGestureDetector?): Boolean {
            var size: Float = textSize
            Log.d("TextSizeStart", size.toString())

            val factor = detector!!.scaleFactor
            Log.d("Factor", factor.toString())


            val product = size * factor
            Log.d("TextSize", product.toString())
            setTextSize(TypedValue.COMPLEX_UNIT_PX, product)

            size = textSize
            Log.d("TextSizeEnd", size.toString())
            return true
        }
    }
    private val scaleGestureDetector = ScaleGestureDetector(context, scaleListener)

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        scaleGestureDetector.onTouchEvent(event)
        return true
    }

}