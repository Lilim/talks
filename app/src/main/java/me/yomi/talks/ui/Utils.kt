package me.yomi.talks.ui

import me.yomi.talks.R

object Utils {

    fun getColorByName(name: String?): Pair<Int, Int> {
        return when (name) {
            "blue" -> Pair(R.color.trackblue, R.color.white)
            "red" -> Pair(R.color.trackred, R.color.white)
            "yellow" -> Pair(R.color.trackyellow, R.color.black)
            "green" -> Pair(R.color.trackgreen, R.color.black)
            "violet" -> Pair(R.color.trackviolet, R.color.white)
            "darkblue" -> Pair(R.color.trackdarkblue, R.color.white)
            "grey" -> Pair(R.color.medium_dark_gray, R.color.black)
            else -> Pair(R.color.medium_dark_gray, R.color.black)
        }
    }
}