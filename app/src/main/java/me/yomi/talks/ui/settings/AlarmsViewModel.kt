package me.yomi.talks.ui.settings

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import me.yomi.talks.core.database.DatabaseRepository
import me.yomi.talks.core.database.entities.EventEntity

class AlarmsViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: DatabaseRepository = DatabaseRepository(application, viewModelScope)
    val alarmsList  = MutableLiveData<List<EventEntity>>()

    fun getAlarms() = viewModelScope.launch {
        val list = repository.getAlarms()
        val tempList: List<String> = list.map {
            it.guid
        }
        val eventList = repository.getEventByGuid(tempList)
        alarmsList.value = eventList
    }
}