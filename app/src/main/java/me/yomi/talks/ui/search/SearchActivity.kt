package me.yomi.talks.ui.search

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import me.yomi.talks.R
import me.yomi.talks.databinding.SearchMainBinding
import me.yomi.talks.ui.ActivityOperationsListener
import me.yomi.talks.ui.ActivityWithDetails
import me.yomi.talks.ui.FavoritesChangedListener
import me.yomi.talks.ui.detail.DetailFragment
import me.yomi.talks.ui.detail.OpenDetailFragmentListener
import me.yomi.talks.ui.room.EventAdapter

class SearchActivity : ActivityWithDetails(),
    OpenDetailFragmentListener, FavoritesChangedListener, ActivityOperationsListener {
    private lateinit var searchModel: SearchViewModel
    private lateinit var binding: SearchMainBinding
    private val manager: FragmentManager = supportFragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchModel =
            ViewModelProvider(
                this,
                SearchViewModelFactory(application)
            ).get(SearchViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.search_main)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)

        title = getString(R.string.search)

        val searchFragment = SearchFragment()
        searchFragment.setBackButtonListener(this)
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragmentContainer, searchFragment)
        transaction.addToBackStack("search")
        transaction.commit()

        invalidateOptionsMenu()
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        super.onOptionsItemSelected(item)
        println("## itemid " + item?.itemId)
        if (item?.itemId == android.R.id.home) {
            println("## finish?1")
            finish()
        }
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        return false
    }

    override fun openDetailFragment(detailFragment: DetailFragment, tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openDetailActivity(guid: String, tag: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onFavoritesChanged(guid: String, insert: Boolean, room: String, origin: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun closeSubview() {
        val fragment = supportFragmentManager.findFragmentById(R.id.fragmentContainer)
        if (fragment is DetailFragment) {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun openedSubview(name: String) {
//        searchView.onActionViewCollapsed()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.details)
        searchModel.subViewOpened = true
    }

}