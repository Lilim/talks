package me.yomi.talks.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import me.yomi.talks.core.database.DatabaseRepository

class HomeViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: DatabaseRepository = DatabaseRepository(application, viewModelScope)
    val roomsList = MutableLiveData<List<String>>()
    val favoritesList = MutableLiveData<List<String>>()
    var currentDate: String = ""
    var initialSetupDone = false

    fun getRooms() = viewModelScope.launch {
        val favList = repository.getFavorites()
        favoritesList.value = favList

        val list = repository.getRooms()
        val tempList = ArrayList<String>()
        if (!favList.isNullOrEmpty()) {
            tempList.add("Favorites")
        }
        tempList.add("All")
        tempList.addAll(list)
        roomsList.value = tempList
    }

    fun changeFavoriteInDatabase(guid: String, insert: Boolean) = runBlocking {
        if (insert) {
            repository.insertFavorite(guid)
        } else {
            repository.deleteFavorite(guid)
        }
        val list = repository.getFavorites()
        favoritesList.value = list
    }
}