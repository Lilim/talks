package me.yomi.talks.ui.detail

interface OpenDetailFragmentListener {
    fun openDetailFragment(detailFragment: DetailFragment, tag: String)
    fun openDetailActivity(guid: String, tag: String)
}