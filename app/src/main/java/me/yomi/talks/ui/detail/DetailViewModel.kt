package me.yomi.talks.ui.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import me.yomi.talks.core.database.DatabaseRepository
import me.yomi.talks.core.database.entities.EventEntity

class DetailViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: DatabaseRepository = DatabaseRepository(application, viewModelScope)
    val event = MutableLiveData<EventEntity>()

    fun getEventByGuid(guid: String) = viewModelScope.launch {
        val list = repository.getEventByGuid(listOf(guid))
        event.postValue(list[0])
        // TODO get alarm from db to change icon
    }

    fun changeFavorite(isNowFavorite: Boolean, guid: String) = viewModelScope.launch {
        if (isNowFavorite) {
            repository.insertFavorite(guid)
        } else {
            repository.deleteFavorite(guid)
        }
    }

    fun insertAlarm(guid: String, requestCode: Int, startTime: Long, endTime: Long) = viewModelScope.launch {
        repository.insertAlarm(guid, requestCode, startTime, endTime)
    }
}