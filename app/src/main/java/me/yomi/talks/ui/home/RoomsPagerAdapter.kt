package me.yomi.talks.ui.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import me.yomi.talks.core.Constants
import me.yomi.talks.core.prefs
import me.yomi.talks.ui.FavoritesChangedListener
import me.yomi.talks.ui.custom.Update
import me.yomi.talks.ui.custom.ViewPagerFragment
import me.yomi.talks.ui.room.RoomFragment
import me.yomi.talks.ui.schedule.ScheduleFragment

class RoomsPagerAdapter(
    private val fragmentManager: FragmentManager,
    private val favoritesChangedListener: FavoritesChangedListener
) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private var rooms: ArrayList<String> = ArrayList()
    private var currentDate: String = ""

    override fun getItem(position: Int): Fragment {
        return if (rooms[position] == Constants.FAVORITES) {
            if (prefs.favlayout == Constants.LIST_LAYOUT) {
                createRoomFragment(position)
            } else {
                createScheduleFragment(position, true)
            }
        } else {
            if (prefs.roomlayout == Constants.SCHEDULE_LAYOUT) {
                createScheduleFragment(position, false)
            } else {
                createRoomFragment(position)
            }
        }
    }

    private fun createRoomFragment(position: Int): Fragment {
        val roomFragment = RoomFragment.newInstance(rooms!![position], currentDate) as RoomFragment
        roomFragment.setFavoritesChangedListener(favoritesChangedListener)
        return roomFragment
    }

    private fun createScheduleFragment(position: Int, canOverlap: Boolean): Fragment {
        val scheduleFragment = ScheduleFragment()
            .newInstance(rooms[position], currentDate, canOverlap)
        scheduleFragment.setFavoritesChangedListener(favoritesChangedListener)
        return scheduleFragment
    }

    fun addFavoriteFragment() {
        rooms.add(0, Constants.FAVORITES)
    }

    fun removeFavoriteFragment() {
        if (!rooms.isNullOrEmpty() && rooms[0] == Constants.FAVORITES) {
            rooms.removeAt(0)
        }
    }

    fun setRooms(newRooms: List<String>) {
        rooms.clear()
        rooms.addAll(newRooms)
        notifyDataSetChanged()
    }

    fun setCurrentDate(newDate: String) {
        this.currentDate = newDate
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return rooms.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return rooms[position]
    }

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    fun updateFavoriteFragment(guid: String, insert: Boolean) {
        for (fragment in fragmentManager.fragments) {
            if ((fragment as ViewPagerFragment).getRoomName() == Constants.FAVORITES) {
                if (prefs.favlayout == Constants.LIST_LAYOUT) {
                    fragment.updateFavoriteInView(guid, insert)
                } else {
                    fragment.updateFavoriteInView(guid, insert)
                }
            }
        }
    }

    fun updateFragmentByRoomName(guid: String, insert: Boolean, roomName: String) {
        for (fragment in fragmentManager.fragments) {
            if ((fragment as ViewPagerFragment).getRoomName() == roomName) {
                if (prefs.favlayout == Constants.LIST_LAYOUT) {
                    fragment.updateFavoriteInView(guid, insert)
                } else {
                    fragment.updateFavoriteInView(guid, insert)
                }
            }
        }
    }
}