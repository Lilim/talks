package me.yomi.talks.ui.schedule

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.schedule_fragment.*
import me.yomi.talks.R
import me.yomi.talks.core.Constants
import me.yomi.talks.databinding.ScheduleFragmentBinding
import me.yomi.talks.ui.FavoritesChangedListener
import me.yomi.talks.ui.custom.ViewPagerFragment
import me.yomi.talks.ui.detail.DetailFragment
import me.yomi.talks.ui.detail.OpenDetailFragmentListener
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class ScheduleFragment : ViewPagerFragment(),
    OpenDetailFragmentListener {
    private lateinit var binding: ScheduleFragmentBinding
    private lateinit var scheduleViewModel: ScheduleViewModel
    private lateinit var scheduleViewCreator: ScheduleViewCreator
    private lateinit var favoritesChangedListener: FavoritesChangedListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.schedule_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        scheduleViewModel = ViewModelProviders.of(this)[ScheduleViewModel::class.java]

        scheduleViewModel.eventList.observe(this, Observer { list ->
            if (list != null) {
                if (scheduleViewModel.canOverlap) {
                    doAsync {
                        uiThread {
                            scheduleViewCreator = ScheduleViewCreator(scheduleViewModel)
                            scheduleViewCreator.preCalculateLeftMargin(list)
                            if (activity != null) {
                                scheduleViewCreator.createEvents(
                                    list,
                                    binding.eventContainer,
                                    activity!!.applicationContext,
                                    this@ScheduleFragment,
                                    favoritesChangedListener
                                )
                            }
                            scheduleViewCreator.postCalculateMargin(list, binding.eventContainer)
                        }
                    }
                } else {
                    doAsync {
                        uiThread {
                            scheduleViewCreator = ScheduleViewCreator(scheduleViewModel)
                            if (activity != null) {
                                scheduleViewCreator.createEvents(
                                    list,
                                    binding.eventContainer,
                                    activity!!.applicationContext,
                                    this@ScheduleFragment,
                                    favoritesChangedListener
                                )
                            }
                        }
                    }
                }
            }
        })

        val args = arguments
        if (args != null) {
            super.setRoomName(Constants.FAVORITES)
            scheduleViewModel.canOverlap = args.getBoolean("canOverlap", true)
            scheduleViewModel.room = args.getString("room", "")
            scheduleViewModel.currentDate = args.getString("date", "")
        }

        setUpViewDimensions()

        getEvents()

        return binding.root
    }

    private fun getEvents() {
        if (binding.eventContainer.childCount > 0) {
            binding.eventContainer.removeAllViews()
        }
        scheduleViewModel.getEvents(scheduleViewModel.room, scheduleViewModel.currentDate)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpTimeTable()
    }

    private fun setUpTimeTable() {
        val timeTableCreator = ScheduleTimeTableCreator(context!!, scheduleViewModel)
        for (i in 0..scheduleViewModel.hoursOfCongressDay) {
            timeTableCreator.setUpTime(timesContainer)
            timeTableCreator.setUpRow(backgroundRowsContainer)
        }
    }

    fun newInstance(room: String, date: String, canOverlap: Boolean): ScheduleFragment {
        val scheduleFragment = ScheduleFragment()
        val args = Bundle()
        args.putBoolean("canOverlap", canOverlap)
        args.putString("room", room)
        args.putString("date", date)
        scheduleFragment.arguments = args
        return scheduleFragment
    }

    private fun setUpViewDimensions() {
        scheduleViewModel.timeHeight =
            context!!.resources.getDimensionPixelSize(R.dimen.heightEventDay)
        scheduleViewModel.timeWidth =
            context!!.resources.getDimensionPixelSize(R.dimen.widthEventDay)
        scheduleViewModel.minuteHeight = scheduleViewModel.timeHeight.toDouble() / 60
        scheduleViewModel.defaultMargin =
            context!!.resources.getDimensionPixelSize(R.dimen.padding20dp)
        scheduleViewModel.dividerMargin =
            context!!.resources.getDimensionPixelSize(R.dimen.padding3dp)
    }

    override fun openDetailFragment(detailFragment: DetailFragment, tag: String) {
        val fm = activity?.supportFragmentManager
        fm?.beginTransaction()
            ?.add(R.id.fragmentContainer, detailFragment, tag)
            ?.addToBackStack(tag)
            ?.commit()
    }

    override fun openDetailActivity(guid: String, tag: String) {}

    override fun updateFavoriteInView(guid: String, insert: Boolean) {
        // Update favorite in list
//        scheduleViewModel.changeFavoriteInDatabase(guid, insert)
        // get and remove event view
        if (::scheduleViewModel.isInitialized) {
            println("## updatefavriteinview " + insert)
            getEvents()
        } else {
            println("## not init scheduleViewModel")
        }
    }

    fun setFavoritesChangedListener(favoritesChangedListener: FavoritesChangedListener) {
        this.favoritesChangedListener = favoritesChangedListener
    }
}