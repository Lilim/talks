package me.yomi.talks.ui.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import me.yomi.talks.R
import me.yomi.talks.core.Constants
import me.yomi.talks.core.prefs
import me.yomi.talks.databinding.SettingsFragmentBinding
import me.yomi.talks.ui.ActivityOperationsListener
import me.yomi.talks.ui.ActivityWithDetails

class SettingsFragment : Fragment() {
    private lateinit var binding: SettingsFragmentBinding
    private lateinit var settingsViewModel: SettingsViewModel
    private lateinit var adapter: ColorSettingAdapter
    private var favcolor: Int = 0
    private var favlayout: Int = 0
    private var roomlayout: Int = 0
    private var roomcolor: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.settings_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        settingsViewModel = ViewModelProviders.of(this)[SettingsViewModel::class.java]

        // Save prestate
        favlayout = prefs.favlayout
        favcolor = prefs.favcolor
        roomlayout = prefs.roomlayout
        roomcolor = prefs.roomcolor

        adapter = ColorSettingAdapter(context!!)
//        binding.colorSettings.adapter = adapter
//        binding.colorSettings.layoutManager = LinearLayoutManager(activity)

        settingsViewModel.colorList.observe(this, Observer {
            adapter.setColorList(it)
        })
        settingsViewModel.getColors()

        binding.alarmSettings.setOnClickListener {
            openAlarmFragment()
        }

        (activity as ActivityWithDetails).openedSubview(getString(R.string.settings))

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Preselect
        if (prefs.favlayout == Constants.LIST_LAYOUT) {
            binding.groupFavoriteLayout.check(R.id.layout_list)
        } else {
            binding.groupFavoriteLayout.check(R.id.layout_schedule)
        }

        val color = prefs.favcolor
        binding.groupFavoriteColor.check(
            when (color) {
                Constants.COLOR_GREEN -> R.id.color_green
                Constants.COLOR_TRACK -> R.id.color_track
                else -> R.id.color_blue
            }
        )

        if (prefs.roomlayout == Constants.SCHEDULE_LAYOUT) {
            binding.groupRoomLayout.check(R.id.roomlayout_schedule)
        } else {
            binding.groupRoomLayout.check(R.id.roomlayout_list)
        }

        val colorroom = prefs.roomcolor
        binding.groupRoomColor.check(
            when (colorroom) {
                Constants.COLOR_GREEN -> R.id.roomcolor_green
                Constants.COLOR_TRACK -> R.id.roomcolor_track
                else -> R.id.roomcolor_blue
            }
        )

        // Save choices
        binding.groupFavoriteLayout.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.layout_list -> prefs.sharedPrefs.edit().putInt(
                    Constants.FAVORITE_LAYOUT,
                    Constants.LIST_LAYOUT
                ).apply()
                R.id.layout_schedule -> prefs.sharedPrefs.edit().putInt(
                    Constants.FAVORITE_LAYOUT,
                    Constants.SCHEDULE_LAYOUT
                ).apply()
            }
        }

        binding.groupFavoriteColor.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.color_blue -> prefs.sharedPrefs.edit().putInt(
                    Constants.FAVORITE_COLOR,
                    Constants.COLOR_BLUE
                ).apply()
                R.id.color_green -> prefs.sharedPrefs.edit().putInt(
                    Constants.FAVORITE_COLOR,
                    Constants.COLOR_GREEN
                ).apply()
                R.id.color_track -> prefs.sharedPrefs.edit().putInt(
                    Constants.FAVORITE_COLOR,
                    Constants.COLOR_TRACK
                ).apply()
            }
        }

        binding.groupRoomLayout.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.roomlayout_list -> prefs.sharedPrefs.edit().putInt(
                    Constants.ROOM_LAYOUT,
                    Constants.LIST_LAYOUT
                ).apply()
                R.id.roomlayout_schedule -> prefs.sharedPrefs.edit().putInt(
                    Constants.ROOM_LAYOUT,
                    Constants.SCHEDULE_LAYOUT
                ).apply()
            }
        }

        binding.groupRoomColor.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.roomcolor_blue -> prefs.sharedPrefs.edit().putInt(
                    Constants.ROOM_COLOR,
                    Constants.COLOR_BLUE
                ).apply()
                R.id.roomcolor_green -> prefs.sharedPrefs.edit().putInt(
                    Constants.ROOM_COLOR,
                    Constants.COLOR_GREEN
                ).apply()
                R.id.roomcolor_track -> prefs.sharedPrefs.edit().putInt(
                    Constants.ROOM_COLOR,
                    Constants.COLOR_TRACK
                ).apply()
            }
        }
    }

    fun checkForChanges(): Boolean {
        var changed = false

        if (prefs.sharedPrefs.getInt(
                Constants.FAVORITE_LAYOUT,
                Constants.SCHEDULE_LAYOUT
            ) != favlayout
        ) {
            changed = true
        }

        if (prefs.sharedPrefs.getInt(Constants.ROOM_LAYOUT, Constants.LIST_LAYOUT) != roomlayout) {
            changed = true
        }

        if (prefs.sharedPrefs.getInt(Constants.FAVORITE_COLOR, Constants.COLOR_BLUE) != favcolor) {
            changed = true
        }

        if (prefs.sharedPrefs.getInt(Constants.ROOM_COLOR, Constants.COLOR_TRACK) != roomcolor) {
            changed = true
        }
        return changed
    }

    override fun onDestroy() {
        super.onDestroy()

        (activity as ActivityWithDetails).closedSubview()
    }

    fun openAlarmFragment() {
        val tag = "alarms"
        val alarmsFragment = AlarmsFragment()
        val fm = activity?.supportFragmentManager
        fm?.beginTransaction()
            ?.add(R.id.fragmentContainer, alarmsFragment, tag)
            ?.addToBackStack(tag)
            ?.commit()
    }
}