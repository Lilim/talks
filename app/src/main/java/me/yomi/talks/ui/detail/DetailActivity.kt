package me.yomi.talks.ui.detail

import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import me.yomi.talks.R
import me.yomi.talks.databinding.ActivityDetailBinding
import me.yomi.talks.ui.BaseActivity
import me.yomi.talks.ui.FavoritesChangedListener

class DetailActivity : BaseActivity(), FavoritesChangedListener {
    private lateinit var binding: ActivityDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)

        setSupportActionBar(binding.myToolbar)

        title = getString(R.string.details)

        var guid = ""
        if (intent.extras != null) {
            guid = intent.getStringExtra("guid").orEmpty()
        }

        if (guid.isNotEmpty()) {
            val detailFragment = DetailFragment.newInstance(guid, false)
            detailFragment.setFavoritesChangedListener(this)
            supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, detailFragment)
                .addToBackStack("details")
                .commit()

            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount <= 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    override fun onFavoritesChanged(guid: String, insert: Boolean, room: String, origin: String) {
        //
    }
}