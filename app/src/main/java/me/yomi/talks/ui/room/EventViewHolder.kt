package me.yomi.talks.ui.room

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.event_item.view.*
import me.yomi.talks.R
import me.yomi.talks.core.Constants
import me.yomi.talks.core.database.entities.EventEntity
import me.yomi.talks.core.utils.Utils
import java.util.*

class EventViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindItem(item: EventEntity, context: Context, origin: String) {
        itemView.title.text = item.title

        if (item.guid != "header") {
            if (item.subtitle.isNotEmpty()) {
                itemView.subtitle.visibility = View.VISIBLE
                itemView.subtitle.text = item.subtitle
            } else {
                itemView.subtitle.text = ""
                itemView.subtitle.visibility = View.GONE
            }

            if (item.isFavorite) {
                itemView.favorite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_star_24px))
            } else {
                itemView.favorite.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_star_border_24px))
            }

            // Format end time
            val calendarEnd = Calendar.getInstance()
            calendarEnd.timeInMillis = item.timestampEnd
            val end = String.format(
                "%02d:%02d",
                calendarEnd.get(Calendar.HOUR_OF_DAY),
                calendarEnd.get(Calendar.MINUTE)
            )

            var time = item.start + " - " + end
            if (origin == Constants.FAVORITES || origin == Constants.ALL) {
                time += " " + item.room
            }
            itemView.time.text = time
            itemView.track.text = "${item.track} [${item.language}]"
            itemView.speaker.text = item.persons?.joinToString {
                it.public_name
            }
            Utils().colorizeTracks(itemView.track, item.track, context)
        }
    }
}