package me.yomi.talks.ui.detail

import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import me.yomi.talks.R
import me.yomi.talks.core.Constants
import me.yomi.talks.core.alarms.AlarmReceiver
import me.yomi.talks.core.alarms.NotificationUtils
import me.yomi.talks.core.database.entities.EventEntity
import me.yomi.talks.core.prefs
import me.yomi.talks.core.utils.Utils
import me.yomi.talks.databinding.DetailFragmentBinding
import me.yomi.talks.ui.ActivityOperationsListener
import me.yomi.talks.ui.ActivityWithDetails
import me.yomi.talks.ui.FavoritesChangedListener
import java.io.IOException
import java.util.*

class DetailFragment : Fragment() {
    private lateinit var binding: DetailFragmentBinding
    private lateinit var detailViewModel: DetailViewModel
    private lateinit var favoritesChangedListener: FavoritesChangedListener
    private var guid: String = ""
    private var isSubView = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
        binding = DataBindingUtil.inflate(inflater, R.layout.detail_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        detailViewModel = ViewModelProviders.of(this)[DetailViewModel::class.java]

        val args = arguments
        if (args != null) {
            guid = args.getString(Constants.ID, "")
            isSubView = args.getBoolean(Constants.IS_SUBVIEW, false)
        }

        if (prefs.sharedPrefs.getBoolean(Constants.SHOW_ALARMS_HINT, true)) {
            binding.alarmNoticeContainer.visibility = View.VISIBLE

            binding.hideAlarmNotice.setOnClickListener {
                // Never show container again
                prefs.sharedPrefs.edit().putBoolean(Constants.SHOW_ALARMS_HINT, false).apply()
                binding.alarmNoticeContainer.visibility = View.GONE
            }
        }

        detailViewModel.event.observe(this, Observer {
            fillData(it)
        })

        if (isSubView) {
            (activity as ActivityWithDetails).openedSubview(getString(R.string.details))
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        detailViewModel.getEventByGuid(guid)
    }

    private fun setAlarm() {
        // TODO check if alarm is set in DB
        // and get intent to cancel alarm

        val event = detailViewModel.event.value
        if (event != null) {
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = event.timestampStart
            val start = String.format(
                "%02d:%02d",
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE)
            )
            calendar.timeInMillis = event.timestampEnd
            val end = String.format(
                "%02d:%02d",
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE)
            )

            val puffer = prefs.sharedPrefs.getLong(Constants.ALARM_PUFFER, 15)

            val room = detailViewModel.event.value!!.room
            val message = "$start - $end in $room"
            try {
                NotificationUtils().setNotification(
                    detailViewModel.event.value!!.timestampStart - (36000 * puffer),
                    activity!!,
                    event.guid,
                    event.title,
                    message,
                    event.subtitle,
                    event.id.toInt()
                )
                detailViewModel.insertAlarm(event.guid, event.id.toInt(), event.timestampStart, event.timestampEnd)
            } catch (exception: IOException) {
                Log.d("DetailFragment", "Error while creating Notification. " + exception.message)
            }
        }
    }

    private fun checkAlarm() {
        val alarmUp = (PendingIntent.getBroadcast(
            context,
            0,
            Intent(activity?.applicationContext, AlarmReceiver::class.java),
            PendingIntent.FLAG_NO_CREATE
        ) != null)

        if (alarmUp) {
            Log.d("DetailFragment", "Alarm is already active")
        } else {
            Log.d("DetailFragment", "Alarm is set again")
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.detail, menu)

        val favItem = menu.findItem(R.id.action_favorite)
        if (favItem != null) {
            if (detailViewModel.event.value != null && detailViewModel.event.value!!.isFavorite) {
                favItem.setIcon(R.drawable.ic_baseline_star_24px)
            } else {
                favItem.setIcon(R.drawable.ic_baseline_star_border_white)
            }
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.action_alarm -> {
                setAlarm()
                item.setIcon(R.drawable.ic_alarm_on_white)
            }
            R.id.action_favorite -> {
                if (detailViewModel.event.value!!.isFavorite) {
                    detailViewModel.event.value!!.isFavorite = false
                    detailViewModel.changeFavorite(false, guid)
                    favoritesChangedListener.onFavoritesChanged(guid, false, detailViewModel.event.value!!.room, "detail")
                    item.setIcon(R.drawable.ic_baseline_star_border_white)
                } else {
                    detailViewModel.event.value!!.isFavorite = true
                    detailViewModel.changeFavorite(true, guid)
                    favoritesChangedListener.onFavoritesChanged(guid, true, detailViewModel.event.value!!.room, "detail")
                    item.setIcon(R.drawable.ic_baseline_star_24px)
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        if (isSubView) {
            (activity as ActivityWithDetails).closedSubview()
        }
    }

    companion object {
        fun newInstance(guid: String, isSubView: Boolean): DetailFragment {
            val detailFragment = DetailFragment()
            val args = Bundle()
            args.putString(Constants.ID, guid)
            args.putBoolean(Constants.IS_SUBVIEW, isSubView)
            detailFragment.arguments = args
            return detailFragment
        }
    }

    private fun fillData(event: EventEntity) {
        binding.title.text = event.title
        if (event.subtitle.isNotEmpty()) {
            binding.subtitle.text = event.subtitle
            binding.subtitle.visibility = View.VISIBLE
        }
        binding.day.text = event.day
        binding.start.text = event.start
        binding.duration.text = event.duration
        binding.room.text = event.room
        binding.track.text = event.track
        Utils().colorizeTracks(binding.track, event.track, context!!)
        binding.language.text = event.language
        binding.abstractText.text = createHtmlText(event.abstractString)
        binding.abstractText.movementMethod = LinkMovementMethod.getInstance()
        binding.description.text = createHtmlText(event.description)
        binding.description.movementMethod = LinkMovementMethod.getInstance()

        if (event.isFavorite) {
            activity?.invalidateOptionsMenu()
        }

        // Create views for linking profile
        /**
        if (event.persons != null) {
        for (person in event.persons!!) {
        binding.speakersContainer.addView(createSpeakerView(person.public_name))
        }
        }
         **/

        binding.speaker.text = event.persons?.joinToString {
            it.public_name
        }
    }

    private fun createHtmlText(text: String): Spanned {
        return if (text.isNotEmpty() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(text)
        }
    }

    /**
    private fun createSpeakerView(speaker: String): TextView {
    val tv = TextView(context)
    val padding3dp = context!!.resources.getDimensionPixelSize(R.dimen.padding3dp)
    tv.setPadding(padding3dp, padding3dp, padding3dp, padding3dp)
    tv.setBackgroundColor(resources.getColor(R.color.blue35c3))
    tv.setTextColor(resources.getColor(R.color.white))
    tv.textSize =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 4F, resources.displayMetrics)
    tv.text = speaker
    val lp = LinearLayout.LayoutParams(
    LinearLayout.LayoutParams.WRAP_CONTENT,
    LinearLayout.LayoutParams.WRAP_CONTENT
    )
    lp.bottomMargin = context!!.resources.getDimensionPixelSize(R.dimen.padding5dp)
    tv.layoutParams = lp
    return tv
    }
     **/

    fun setFavoritesChangedListener(favoritesChangedListener: FavoritesChangedListener) {
        this.favoritesChangedListener = favoritesChangedListener
    }
}