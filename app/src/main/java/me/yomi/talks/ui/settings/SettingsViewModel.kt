package me.yomi.talks.ui.settings

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import me.yomi.talks.core.database.DatabaseRepository
import me.yomi.talks.core.database.entities.ColorEntity

class SettingsViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: DatabaseRepository = DatabaseRepository(application, viewModelScope)
    val colorList  = MutableLiveData<List<ColorEntity>>()

    fun getColors() = viewModelScope.launch {
        val list = repository.getColors()
        println("## colorlist "+list.size)
        colorList.value = list
    }
}