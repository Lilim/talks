package me.yomi.talks

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import me.yomi.talks.core.Constants
import me.yomi.talks.core.database.DatabaseRepository
import me.yomi.talks.core.database.entities.*
import me.yomi.talks.core.network.ApiFactory
import me.yomi.talks.core.network.NetworkRepository
import me.yomi.talks.core.network.models.RoomsDataResponse
import me.yomi.talks.core.network.models.ScheduleResponse
import me.yomi.talks.core.prefs
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: DatabaseRepository = DatabaseRepository(application, viewModelScope)
    private val networkRepository = NetworkRepository(ApiFactory.congressAPI)
    private val networkRepositoryOld = NetworkRepository(ApiFactory.congressAPIOld)
    private val favoritesList = ArrayList<String>()
    private var conferenceUpdated = false
    private var daysUpdated = false
    private var eventsUpdated = false
    private var colorsUpdated = false
    val daysList = MutableLiveData<List<String>>()
    val updating = MutableLiveData<Boolean>()
    var today: String = ""
    var currentDate: String = ""
    var error = MutableLiveData<Boolean>(false)

    fun fetchData() {
        updating.value = true
        CoroutineScope(Dispatchers.IO).launch {
            val response = networkRepository.getDataFromNetworkAsync()
            withContext(Dispatchers.Main) {
                try {
                    if (response!!.isSuccessful) {
                        val version = repository.getVersion()
                        if (response.body()!!.schedule.version != version) {
                            val list = repository.getFavorites()
                            favoritesList.addAll(list)
                            resetUpdatedFlags()
                            formatData(response.body()!!)
                        } else {
                            println("## don't update")
                            updating.value = false
                        }
                    } else {
                        Log.d("Network Repo", "## Error")
                        error.value = true
                    }
                } catch (eso: SocketTimeoutException) {
                    Log.d("Network Repo", "## TIMEOUT")
                    error.value = true
                } catch (e: Throwable) {
                    Log.d("Network Repo", "## oh oh " + e.message)
                    error.value = true
                }
            }
        }
    }

    fun fetchOldData() {
        updating.value = true
        CoroutineScope(Dispatchers.IO).launch {
            val response = networkRepositoryOld.getOldDataFromNetworkAsync()
            withContext(Dispatchers.Main) {
                try {
                    if (response!!.isSuccessful) {
                        val version = repository.getVersion()
                        val list = repository.getFavorites()
                        favoritesList.addAll(list)
                        if (response.body()!!.schedule.version != version) {
                            formatData(response.body()!!)
                        } else {
                            println("## don't update")
                            getDays()
//                            getFavorites()
                        }
                    } else {
                        Log.d("Network Repo", "## Error")
                    }
                } catch (e: HttpException) {
                    Log.d("Network Repo", "httpex")
                } catch (eso: SocketTimeoutException) {
                    Log.d("Network Repo", "## TIMEOUT")
                } catch (e: Throwable) {
                    Log.d("Network Repo", "## oh oh " + e.message)
                } finally {
                    updating.value = false
                    getDays()
                }
            }
        }
    }

    fun getDays() = viewModelScope.launch {
        val list = repository.getDays()
        daysList.postValue(list)
    }

    private fun createColorResourceForRooms() = viewModelScope.launch {
        val list = repository.getRooms()
        val colorList = listOf("blue", "red", "yellow", "green", "violet", "darkblue", "grey")

        for ((index, room) in list.withIndex()) {
            var bgColor = "white"
            if (index < colorList.size) {
                bgColor = colorList[index]
            }
            val id = repository.insertColor(
                ColorEntity(
                    room,
                    bgColor
                )
            )
            colorsUpdated = true
            checkForUpdate()
        }
    }

    private fun formatData(response: ScheduleResponse) {
        viewModelScope.launch {
            repository.clearConferenceData()
            val id = repository.insertConferenceData(
                ConferenceEntity(
                    response.schedule.conference.title,
                    response.schedule.version,
                    response.schedule.conference.start,
                    response.schedule.conference.end,
                    response.schedule.conference.daysCount
                )
            )
            conferenceUpdated = true
            checkForUpdate()
            Log.d("MainViewModel", "Conference data inserted " + id)
        }

        val days = response.schedule.conference.days
        val listOfDays = ArrayList<DayEntity>()

        for (day in days) {
            day.rooms.forEach { (_, value) ->
                if (value != null && value.isNotEmpty()) {
                    createAndSaveEvents(value)
                }
            }
            listOfDays.add(DayEntity(day.index, day.date, day.day_start, day.day_end))
        }
        eventsUpdated = true

        // Save colors for rooms
        try {
            createColorResourceForRooms()
        } catch (exception: IOException) {
            Log.d("MainViewModel", "FormatData " + exception.message)
        }

        // Save days
        viewModelScope.launch {
            val list = repository.insertDays(listOfDays)
            daysUpdated = true
            checkForUpdate()
        }
        prefs.sharedPrefs.edit().putLong(Constants.UPDATE, System.currentTimeMillis()).apply()
    }

    private fun createAndSaveEvents(room: Array<RoomsDataResponse>?) {
        val list: ArrayList<EventEntity> = ArrayList()
        for (event in room.orEmpty()) {

            // Create timestamp
            val datetime =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()).parse(event.date)
            val timestampStart = datetime.time

            val split = event.duration.split(":")
            val hours = split[0].toInt() * 60 * 60 * 1000
            val minutes = split[1].toInt() * 60 * 1000
            val timestampEnd = timestampStart + hours + minutes

            // Congress day is not a normal day
            // Find out if event starts before 5 AM then put it to the event day before
            val calendar = Calendar.getInstance()
            calendar.time = datetime!!
            if (calendar.get(Calendar.HOUR_OF_DAY) < 5) {
                calendar.add(Calendar.DATE, -1)
            }
            val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val day = formatter.format(calendar.time)

            val persons = event.persons?.joinToString {
                it.public_name
            }

            list.add(
                EventEntity(
                    event.guid,
                    event.id,
                    event.date,
                    event.start,
                    event.duration,
                    event.room,
                    event.title,
                    event.subtitle.orEmpty(),
                    event.track.orEmpty(),
                    event.type,
                    event.language,
                    event.abstract.orEmpty(),
                    event.description.orEmpty(),
                    event.url,
                    event.persons,
                    day,
                    timestampStart,
                    timestampEnd,
                    "${event.room} ${event.title} ${event.subtitle} ${event.track} ${persons}",
                    favoritesList.contains(event.guid)
                )
            )
//            println("## list " + event.title + " " + day)
        }
        insertEvents(list)
    }

    private fun insertEvents(list: ArrayList<EventEntity>) {
        viewModelScope.launch {
            repository.insertEvent(list)
        }
    }

    fun initRoomSort() = viewModelScope.launch {
        val list = listOf(
            RoomSortEntity("Ada", 1),
            RoomSortEntity("Borg", 2),
            RoomSortEntity("Clarke", 3),
            RoomSortEntity("Dijkstra", 4),
            RoomSortEntity("Eliza", 5)
        )
        repository.insertRoomSort(list)
    }

    private fun checkForUpdate() {
        if (conferenceUpdated && daysUpdated && eventsUpdated) {
            updating.value = false
            getDays()
        }
    }

    private fun resetUpdatedFlags() {
        conferenceUpdated = false
        eventsUpdated = false
        daysUpdated = false
        colorsUpdated = false
    }
}