package me.yomi.talks.core.utils

import android.content.Context
import android.view.View
import android.widget.TextView
import me.yomi.talks.core.Constants
import me.yomi.talks.R

class Utils {

    fun colorizeTracks(tv: TextView, track: String, context: Context) {
        var bgColor = 0
        var textColor = context.resources.getColor(R.color.white)

        when (track) {
            Constants.ARTCULTURE -> {
                bgColor = context.resources.getColor(R.color.trackyellow)
                textColor = context.resources.getColor(R.color.black)
            }
            Constants.CCC -> bgColor = context.resources.getColor(R.color.trackgrey)
            Constants.ENTERTAINMENT -> bgColor = context.resources.getColor(
                R.color.trackgrey
            )
            Constants.ESP -> bgColor = context.resources.getColor(R.color.trackred)
            Constants.HWMK -> bgColor = context.resources.getColor(R.color.trackviolet)
            Constants.RESILIENCE -> bgColor = context.resources.getColor(R.color.trackblue)
            Constants.SCIENCE -> bgColor = context.resources.getColor(R.color.trackgreen)
            Constants.SECURITY -> bgColor = context.resources.getColor(R.color.trackdarkblue)
            else -> {
                bgColor = context.resources.getColor(R.color.medium_dark_gray)
                textColor = context.resources.getColor(R.color.black)
            }
        }

        tv.setBackgroundColor(bgColor)
        tv.setTextColor(textColor)
    }

    fun colorizeTracks(tv: View, track: String, context: Context) {
        var bgColor = 0

        when (track) {
            Constants.ARTCULTURE -> bgColor =
                R.drawable.round_corner_yellow
            Constants.CCC -> bgColor =
                R.drawable.round_corner_grey
            Constants.ENTERTAINMENT -> bgColor =
                R.drawable.round_corner_grey
            Constants.ESP -> bgColor =
                R.drawable.round_corner_red
            Constants.HWMK -> bgColor =
                R.drawable.round_corner_violet
            Constants.RESILIENCE -> bgColor =
                R.drawable.round_corner_blue
            Constants.SCIENCE -> bgColor =
                R.drawable.round_corner_green
            Constants.SECURITY -> bgColor =
                R.drawable.round_corner_darkblue
        }

        tv.setBackgroundResource(bgColor)
    }
}