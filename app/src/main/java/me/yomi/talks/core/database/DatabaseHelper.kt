package me.yomi.talks.core.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns

class DatabaseHelper(context: Context?) :
    SQLiteOpenHelper(context, DatabaseValues.DB_NAME, null, DatabaseValues.DB_VERSION) {

    object DatabaseValues : BaseColumns {
        val DB_NAME = "talks-db"
        val DB_VERSION = 1

        // Entry
        val TABLE_EVENT = "event"
        val TABLE_FAVORITE = "favorite"
        val _ID = "_id"
        val ID = "id"
        val GUID = "guid"
        val DATE = "date"
        val START = "start"
        val DURATION = "duration"
        val ROOM = "room"
        val TITLE = "title"
        val SUBTITLE = "subtitle"
        val TRACK = "track"
        val TYPE = "type"
        val LANGUAGE = "language"
        val ABSTRACT = "abstract"
        val DESCRIPTION = "description"
        val SPEAKERS = "speakers"
        val URL = "url"
        val TIMESTAMP = "timestamp"
        val DAY = "day"
        val SEARCHABLE = "searchable"
    }

    private val createTableEvent = "CREATE TABLE " + DatabaseValues.TABLE_EVENT +
            "( " + DatabaseValues._ID + " INTEGER PRIMARY KEY," +
            DatabaseValues.ID + " LONG," +
            DatabaseValues.GUID + " TEXT," +
            DatabaseValues.DATE + " TEXT," +
            DatabaseValues.START + " TEXT," +
            DatabaseValues.DURATION + " TEXT," +
            DatabaseValues.ROOM + " TEXT," +
            DatabaseValues.TITLE + " TEXT," +
            DatabaseValues.SUBTITLE + " TEXT," +
            DatabaseValues.TRACK + " TEXT," +
            DatabaseValues.TYPE + " TEXT," +
            DatabaseValues.LANGUAGE + " TEXT," +
            DatabaseValues.ABSTRACT + " TEXT," +
            DatabaseValues.DESCRIPTION + " TEXT," +
            DatabaseValues.SPEAKERS + " TEXT," +
            DatabaseValues.URL + " TEXT," +
            DatabaseValues.TIMESTAMP + " LONG, " +
            DatabaseValues.DAY + " DATE, " +
            DatabaseValues.SEARCHABLE + " TEXT);"

    private val createTableFavorites = "CREATE TABLE " + DatabaseValues.TABLE_FAVORITE +
            "( " + DatabaseValues._ID + " INTEGER PRIMARY KEY," +
            DatabaseValues.GUID + " TEXT)"

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(createTableEvent)
        db?.execSQL(createTableFavorites)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

}