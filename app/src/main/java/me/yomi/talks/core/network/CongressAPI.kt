package me.yomi.talks.core.network

import me.yomi.talks.core.network.models.ScheduleResponse
import retrofit2.Response
import retrofit2.http.GET

interface CongressAPI {
    @GET("everything.schedule.json")
    suspend fun getData(): Response<ScheduleResponse>

    @GET("schedule.json")
    suspend fun getOldData(): Response<ScheduleResponse>
}