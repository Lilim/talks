package me.yomi.talks.core.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiFactory {

    private fun retrofit(): Retrofit = Retrofit.Builder()
        .baseUrl("https://raw.githubusercontent.com/voc/36C3_schedule/master/")
//        .baseUrl("https://data.c3voc.de/36C3/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val congressAPI = retrofit().create(CongressAPI::class.java)

    private fun retrofitOld(): Retrofit = Retrofit.Builder()
        .baseUrl("https://fahrplan.events.ccc.de/congress/2018/Fahrplan/")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val congressAPIOld = retrofitOld().create(CongressAPI::class.java)
}