package me.yomi.talks.core.alarms

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import me.yomi.talks.MainActivity
import me.yomi.talks.R
import java.util.*

class NotificationService : IntentService(NotificationService::class.java.simpleName) {
    private lateinit var notification: Notification
    private val notificationId: Int = 1000

    companion object {
        const val CHANNEL_ID = "me.yomi.talks"
        const val CHANNEL_NAME = "36c3 events"
    }

    /**
     * Channels are new with Android Oreo therefore
     * only create it for O or higher
     */
    private fun createChannel(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, importance)
            notificationChannel.enableVibration(true)
            notificationChannel.setShowBadge(true)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.MAGENTA
            notificationChannel.description = getString(R.string.notification_channel_description)
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            notificationManager.createNotificationChannel(notificationChannel)
        }
    }

    override fun onHandleIntent(intent: Intent?) {
        println("## onhandleintent")
        var timestamp: Long = 0
        var guid = ""
        var title = ""
        var message = ""
        var description = ""
        if (intent != null && intent.extras != null) {
            timestamp = intent.extras!!.getLong("timestamp")
            guid = intent.extras!!.getString("guid").orEmpty()
            title = intent.extras!!.getString("title").orEmpty()
            message = intent.extras!!.getString("message").orEmpty()
            description = intent.extras!!.getString("description").orEmpty()
        }

        if (timestamp > 0 && guid.isNotEmpty()) {

            val context = applicationContext
            val notifyIntent = Intent(this, MainActivity::class.java)

            notifyIntent.putExtra("guid", guid)
            notifyIntent.putExtra("title", title)
            notifyIntent.putExtra("message", message)
            notifyIntent.putExtra("description", description)
            notifyIntent.putExtra("notification", true)

            notifyIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timestamp

            val pendingIntent = PendingIntent.getActivity(
                context,
                0,
                notifyIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
            val ringToneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createChannel(context)

                notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_event_note_24px)
                    .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(description)
                    )

                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .build()
            } else {
                notification = NotificationCompat.Builder(this)
                    // Set the intent that will fire when the user taps the notification
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_event_note_24px)
                    .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setSound(ringToneUri)
                    .setContentText(message)
                    .setStyle(
                        NotificationCompat.BigTextStyle()
                            .bigText(description)
                    )
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .build()
            }
            with(NotificationManagerCompat.from(this)) {
                notify(notificationId, notification)
            }
        }
    }

}