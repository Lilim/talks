package me.yomi.talks.core.database

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import me.yomi.talks.core.database.dao.*
import me.yomi.talks.core.database.entities.*

class DatabaseRepository(application: Application, viewModelScope: CoroutineScope) {
    private val conferenceDao: ConferenceDao
    private val eventDao: EventDao
    private val dayDao: DayDao
    private val favoritesDao: FavoritesDao
    private val colorDao: ColorDao
    private val alarmDao: AlarmDao
    private val roomSortDao: RoomSortDao

    init {
        conferenceDao =
            CongressDatabase.getDatabase(application, viewModelScope).conferenceDao()
        eventDao = CongressDatabase.getDatabase(application, viewModelScope).eventDao()
        dayDao = CongressDatabase.getDatabase(application, viewModelScope).dayDao()
        favoritesDao = CongressDatabase.getDatabase(application, viewModelScope).favoritesDao()
        colorDao = CongressDatabase.getDatabase(application, viewModelScope).colorDao()
        alarmDao = CongressDatabase.getDatabase(application, viewModelScope).alarmDao()
        roomSortDao = CongressDatabase.getDatabase(application, viewModelScope).roomSortDao()
    }

    suspend fun getVersion(): String {
        val conferenceData = conferenceDao.getConferenceData()
        return if (conferenceData.isEmpty()) {
            ""
        } else {
            conferenceData[0].version
        }
    }

    suspend fun insertConferenceData(conferenceEntity: ConferenceEntity): Long {
        return conferenceDao.insert(conferenceEntity)
    }

    suspend fun clearConferenceData() {
        conferenceDao.clearConferenceData()
    }

    suspend fun insertEvent(eventList: List<EventEntity>) {
        eventDao.insert(eventList)
    }

    suspend fun insertDays(daysList: List<DayEntity>): List<Long> {
        return dayDao.insert(daysList)
    }

    suspend fun insertColor(color: ColorEntity) {
        colorDao.insert(color)
    }

    suspend fun getEventsByDay(day: String): List<EventEntity> {
        return eventDao.getEventsByDay(day)
    }

    suspend fun getEventsByRoomAndDay(room: String, day: String): List<EventEntity> {
        return eventDao.getEventsByRoomAndDay(room, day)
    }

    suspend fun getFavorites(): List<String> {
        return favoritesDao.getFavorites()
    }

    suspend fun getFavoritesByDay(day: String): List<EventEntity> {
        val favList = favoritesDao.getFavorites()
        val list = eventDao.getEventsByGuidAndDay(favList, day)
        return list
    }

    suspend fun getRooms(): List<String> {
        return eventDao.getAllRooms()
    }

    suspend fun getDays(): List<String> {
        return dayDao.getDays()
    }

    suspend fun getEventByGuid(guids: List<String>): List<EventEntity> {
        return eventDao.getEventsByGuid(guids)
    }

    suspend fun getColorByRoom(room: String): List<ColorEntity> {
        return colorDao.getColorByRoom(room)
    }

    suspend fun getColors(): List<ColorEntity> {
        return colorDao.getColors()
    }

    suspend fun insertFavorite(guid: String) {
        val id = favoritesDao.insert(FavoritesEntity(guid))
        eventDao.updateEventIsFavorite(guid, true)
    }

    suspend fun deleteFavorite(guid: String) {
        favoritesDao.deleteFavorite(guid)
        eventDao.updateEventIsFavorite(guid, false)
    }

    suspend fun doSearch(query: String): List<EventEntity> {
        return eventDao.searchEvents("%$query%")
    }

    suspend fun insertAlarm(guid: String, requestCode: Int, startTime: Long, endTime: Long) {
        val id = alarmDao.insert(AlarmEntity(guid, requestCode, startTime, endTime))
    }

    suspend fun deleteAlarmByGuid(guid: String) {
        alarmDao.deleteAlarm(guid)
    }

    suspend fun getAlarms(): List<AlarmEntity> {
        val list = alarmDao.getAlarms()
        println("## alarms " + list.size)
        return list
    }

    suspend fun getAlarmByRequestCode(requestCode: Int): List<AlarmEntity> {
        val list = alarmDao.getAlarmByRequestCode(requestCode)
        println("## alarms " + list.size)
        return list
    }

    suspend fun insertRoomSort(roomSortList: List<RoomSortEntity>) {
        val list = roomSortDao.insert(roomSortList)
        println("## roomsort " + list.toString())
    }
}