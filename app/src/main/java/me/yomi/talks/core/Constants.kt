package me.yomi.talks.core

class Constants {

    companion object {
        val DEFAULTPREFS = "defaultPrefs"
        val FIRSTRUN = "firstrun"
        val PLACEHOLDER_ID = 0
        val ID = "id"
        val IS_SUBVIEW = "isSubView"
        val SEARCHCLOSED = 1
        val FAVORITES = "Favorites"
        val ALARM_PUFFER = "alarmpuffer"
        val ALL = "All"
        val UPDATE = "update"

        val ARTCULTURE = "Art & Culture"
        val CCC = "CCC"
        val ENTERTAINMENT = "Entertainment"
        val ESP = "Ethics, Society & Politics"
        val HWMK = "Hardware & Making"
        val RESILIENCE = "Resilience"
        val SCIENCE = "Science"
        val SECURITY = "Security"

        // Settings
        val FAVORITE_LAYOUT = "favoritelayout"
        val FAVORITE_COLOR = "favoritecolor"
        val ROOM_LAYOUT = "roomlayout"
        val ROOM_COLOR = "roomcolor"
        val SCHEDULE_LAYOUT = 0
        val LIST_LAYOUT = 1
        val COLOR_BLUE = 0
        val COLOR_GREEN = 1
        val COLOR_TRACK = 2

        val SHOW_ALARMS_HINT = "show_alarms_hint"
    }
}