package me.yomi.talks.core.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorites")
class FavoritesEntity(
    @PrimaryKey @ColumnInfo(name = "guid") val guid: String
)