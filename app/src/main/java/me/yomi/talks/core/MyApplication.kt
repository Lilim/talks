package me.yomi.talks.core

import android.app.Application
import me.yomi.talks.core.utils.SharedPrefs

val prefs: SharedPrefs by lazy {
    MyApplication.prefs!!
}

class MyApplication : Application() {
    companion object {
        var prefs: SharedPrefs? = null
    }

    override fun onCreate() {
        prefs = SharedPrefs(applicationContext)
        prefs!!.updateSettingsPrefs()

        super.onCreate()
    }
}