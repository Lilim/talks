package me.yomi.talks.core.alarms

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        println("## onreceive")
        val service = Intent(context, NotificationService::class.java)
        service.putExtra("guid", intent?.getStringExtra("guid"))
        service.putExtra("title", intent?.getStringExtra("title"))
        service.putExtra("message", intent?.getStringExtra("message"))
        service.putExtra("timestamp", intent?.getLongExtra("timestamp", 0))

        context!!.startService(service)

        // TODO clean up alarms in DB
    }
}