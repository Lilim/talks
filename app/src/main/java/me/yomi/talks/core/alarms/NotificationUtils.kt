package me.yomi.talks.core.alarms

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Intent
import java.util.*

class NotificationUtils {

    fun setNotification(
        timestamp: Long,
        activity: Activity,
        guid: String,
        title: String,
        message: String,
        description: String,
        id: Int
    ) {
        // Setup alarm
        if (timestamp > 0) {
            val alarmManager = activity.getSystemService(Activity.ALARM_SERVICE) as AlarmManager
            val alarmIntent = Intent(activity.applicationContext, AlarmReceiver::class.java)

            alarmIntent.putExtra("reason", "notification")
            alarmIntent.putExtra("timestamp", timestamp)
            alarmIntent.putExtra("guid", guid)
            alarmIntent.putExtra("title", title)
            alarmIntent.putExtra("message", message)
            alarmIntent.putExtra("description", description)

            val calendar = Calendar.getInstance()
            calendar.timeInMillis = timestamp

            println(
                "## set alarm to " + calendar.get(Calendar.HOUR_OF_DAY) + ":" + calendar.get(
                    Calendar.MINUTE
                )
            )

            val pendingIntent = PendingIntent.getBroadcast(
                activity,
                id,
                alarmIntent,
                PendingIntent.FLAG_CANCEL_CURRENT
            )
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
        }
    }
}