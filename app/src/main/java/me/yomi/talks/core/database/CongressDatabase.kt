package me.yomi.talks.core.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import me.yomi.talks.core.database.dao.*
import me.yomi.talks.core.database.entities.*

@Database(
    entities = [ConferenceEntity::class, DayEntity::class, EventEntity::class, FavoritesEntity::class, ColorEntity::class, AlarmEntity::class, RoomSortEntity::class],
    version = 1,
    exportSchema = false
)

@TypeConverters(Converters::class)
abstract class CongressDatabase : RoomDatabase() {
    abstract fun conferenceDao(): ConferenceDao
    abstract fun eventDao(): EventDao
    abstract fun dayDao(): DayDao
    abstract fun favoritesDao(): FavoritesDao
    abstract fun colorDao(): ColorDao
    abstract fun alarmDao(): AlarmDao
    abstract fun roomSortDao(): RoomSortDao

    private class CongressDatabaseCallback(
        private val scope: CoroutineScope
    ) : RoomDatabase.Callback() {
        override fun onOpen(db: SupportSQLiteDatabase) {
            super.onOpen(db)
            INSTANCE?.let { database ->
                scope.launch(Dispatchers.IO) {
                    var conferenceDao = database.conferenceDao()
                    var eventDao = database.eventDao()
                    var dayDao = database.dayDao()
                    var favoritesDao = database.favoritesDao()
                    var colorDao = database.colorDao()
                    var alarmDao = database.alarmDao()
                    var roomSortDao = database.roomSortDao()
                }
            }
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: CongressDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): CongressDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    CongressDatabase::class.java,
                    "db-congress"
                )
                    .addCallback(CongressDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}