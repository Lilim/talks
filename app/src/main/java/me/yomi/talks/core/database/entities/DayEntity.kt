package me.yomi.talks.core.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "day")
class DayEntity(
    @PrimaryKey @ColumnInfo(name = "index") val index: Int,
    @ColumnInfo(name = "date") val date: String,
    @ColumnInfo(name = "day_start") val dayStart: String,
    @ColumnInfo(name = "day_end") val dayEnd: String
)