package me.yomi.talks.core.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "color")
class ColorEntity(
    @PrimaryKey @ColumnInfo(name = "room") val room: String,
    @ColumnInfo(name = "background_color") val backgroundColor: String
)