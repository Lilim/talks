package me.yomi.talks.core.network.models

import java.io.Serializable

class ScheduleResponse(
    val schedule: ConferenceResponse
)

class ConferenceResponse(
    val version: String,
    val conference: ConferenceDataResponse
)

class ConferenceDataResponse(
    val title: String,
    val start: String,
    val end: String,
    val daysCount: Int,
    val days: Array<DaysDataResponse>
)

class DaysDataResponse(
    val index: Int,
    val date: String,
    val day_start: String,
    val day_end: String,
    val rooms: Map<String, Array<RoomsDataResponse>?>
)

class RoomsDataResponse(
    var id: Long,
    var guid: String,
    var date: String,
    var start: String,
    var duration: String,
    var room: String,
    var title: String,
    var subtitle: String?,
    var track: String?,
    var type: String,
    var language: String,
    var abstract: String?,
    var description: String?,
    var url: String,
    var persons: Array<Persons>?
)

class Persons(
    val id: Long,
    val public_name: String
) : Serializable