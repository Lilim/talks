package me.yomi.talks.core.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "alarm")
class AlarmEntity(
    @PrimaryKey @ColumnInfo(name = "guid") val guid: String,
    @ColumnInfo(name = "request_code") val requestCode: Int,
    @ColumnInfo(name = "starttime") val startime: Long,
    @ColumnInfo(name = "endtime") val endtime: Long
)