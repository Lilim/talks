package me.yomi.talks.core.network

import android.util.Log
import me.yomi.talks.core.network.models.ScheduleResponse
import retrofit2.Response
import java.io.IOException

class NetworkRepository(private val api: CongressAPI) {

    suspend fun getDataFromNetworkAsync(): Response<ScheduleResponse>? {
        var response: Response<ScheduleResponse>? = null
//        /**
        try {
            response = api.getData()
        } catch (exception: IOException) {
            Log.d("network ", "error! " + exception.message)
            return null
        }
//        **/
        return response
    }

    suspend fun getOldDataFromNetworkAsync(): Response<ScheduleResponse>? {
        var response: Response<ScheduleResponse>? = null
        /**
        try {
        response = api.getOldData()
        } catch (exception: IOException) {
        Log.d("network ", "error!")
        }
        //        **/
        return response
    }
}