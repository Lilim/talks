package me.yomi.talks.core.utils

import android.content.Context
import android.content.SharedPreferences
import me.yomi.talks.core.Constants

class SharedPrefs(context: Context) {

    var sharedPrefs: SharedPreferences =
        context.getSharedPreferences(Constants.DEFAULTPREFS, Context.MODE_PRIVATE)
    var favcolor: Int = 0
    var favlayout: Int = 0
    var roomlayout: Int = 0
    var roomcolor: Int = 0

    fun updateSettingsPrefs() {
        favlayout = sharedPrefs.getInt(Constants.FAVORITE_LAYOUT, Constants.SCHEDULE_LAYOUT)
        roomlayout = sharedPrefs.getInt(Constants.ROOM_LAYOUT, Constants.LIST_LAYOUT)
        roomcolor = sharedPrefs.getInt(Constants.ROOM_COLOR, Constants.COLOR_TRACK)
        favcolor = sharedPrefs.getInt(Constants.FAVORITE_COLOR, Constants.COLOR_BLUE)
    }
}