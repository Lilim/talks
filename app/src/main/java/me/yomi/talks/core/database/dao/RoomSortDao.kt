package me.yomi.talks.core.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import me.yomi.talks.core.database.entities.RoomSortEntity

@Dao
interface RoomSortDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(roomSort: List<RoomSortEntity>): List<Long>

    @Query("SELECT * FROM roomsort ORDER BY sort")
    suspend fun getRoomSort(): List<RoomSortEntity>

    @Query("SELECT * FROM roomsort WHERE room = :room")
    suspend fun getSortByRoom(room: String): List<RoomSortEntity>
}