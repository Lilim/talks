package me.yomi.talks.core.network.models

open class EventItem(
    var internalId: Int,
    var id: Long,
    var guid: String,
    var date: String,
    var start: String,
    var duration: String,
    var room: String,
    var title: String,
    var subtitle: String,
    var track: String,
    var type: String,
    var language: String,
    var abstract: String,
    var description: String?,
    var url: String,
    var speaker: String,
    var favorite: Boolean,
    var timestamp: Long,
    var day: String
)
