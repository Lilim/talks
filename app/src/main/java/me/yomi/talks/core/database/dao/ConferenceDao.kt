package me.yomi.talks.core.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import me.yomi.talks.core.database.entities.ConferenceEntity

@Dao
interface ConferenceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(conferenceEntity: ConferenceEntity): Long

    @Query("SELECT * FROM conference")
    suspend fun getConferenceData(): List<ConferenceEntity>

    @Query("DELETE FROM conference")
    suspend fun clearConferenceData()
}