package me.yomi.talks.core.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import me.yomi.talks.core.database.entities.EventEntity

@Dao
interface EventDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(eventEntities: List<EventEntity>): List<Long>

    @Query("SELECT * FROM event WHERE guid IN (:guid)")
    suspend fun getEventsByGuid(guid: List<String>): List<EventEntity>

    @Query("SELECT * FROM event WHERE guid IN (:guid) AND day = :day ORDER BY timestamp_start")
    suspend fun getEventsByGuidAndDay(guid: List<String>, day: String): List<EventEntity>

    @Query("SELECT * FROM event WHERE room = :room AND day = :day ORDER BY timestamp_start")
    suspend fun getEventsByRoomAndDay(room: String, day: String): List<EventEntity>

    @Query("SELECT * FROM event WHERE day = :day ORDER BY timestamp_start")
    suspend fun getEventsByDay(day: String): List<EventEntity>

    @Query("SELECT DISTINCT event.room FROM event LEFT JOIN roomsort ON event.room = roomsort.room ORDER BY CASE WHEN roomsort.sort IS NULL THEN 50 ELSE roomsort.sort END, roomsort.sort, event.room")
    suspend fun getAllRooms(): List<String>

    @Query("DELETE FROM event WHERE guid = :guid")
    suspend fun deleteEvent(guid: String)

    @Query("UPDATE event SET isFavorite = :isFavorite WHERE guid = :guid")
    suspend fun updateEventIsFavorite(guid: String, isFavorite: Boolean)

    @Query("SELECT * from event WHERE searchable LIKE :query")
    suspend fun searchEvents(query: String): List<EventEntity>
}