package me.yomi.talks.core.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import me.yomi.talks.core.database.entities.ColorEntity

@Dao
interface ColorDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(color: ColorEntity): Long

    @Query("SELECT * FROM color")
    suspend fun getColors(): List<ColorEntity>

    @Query("SELECT * FROM color WHERE room = :room")
    suspend fun getColorByRoom(room: String): List<ColorEntity>
}