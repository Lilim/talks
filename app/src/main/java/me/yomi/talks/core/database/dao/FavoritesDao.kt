package me.yomi.talks.core.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import me.yomi.talks.core.database.entities.FavoritesEntity

@Dao
interface FavoritesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(favorite: FavoritesEntity): Long

    @Query("SELECT * FROM favorites")
    suspend fun getFavorites(): List<String>

    @Query("DELETE FROM favorites WHERE guid = :guid")
    suspend fun deleteFavorite(guid: String)
}