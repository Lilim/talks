package me.yomi.talks.core.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "roomsort")
class RoomSortEntity(
    @PrimaryKey @ColumnInfo(name = "room") val room: String,
    @ColumnInfo(name = "sort") val sort: Int
)