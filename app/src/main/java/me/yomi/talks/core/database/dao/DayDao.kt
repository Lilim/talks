package me.yomi.talks.core.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import me.yomi.talks.core.database.entities.DayEntity

@Dao
interface DayDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(days: List<DayEntity>): List<Long>

    @Query("SELECT * FROM day ORDER BY `index`")
    suspend fun getDaysData(): List<DayEntity>

    @Query("SELECT date FROM day ORDER BY `index`")
    suspend fun getDays(): List<String>
}