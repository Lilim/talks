package me.yomi.talks.core.network

enum class ResponseStatus {
    Success, Error, Loading
}