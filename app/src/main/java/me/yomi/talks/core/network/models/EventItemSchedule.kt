package me.yomi.talks.core.network.models

import me.yomi.talks.core.database.entities.EventEntity

open class EventItemSchedule(
    var eventItem: EventEntity,
    var primary: Boolean = false,
    var indexLL: Int = -1,
    var marginLeft: Int = 0,
    var isIndentedByEventId: Int = 0
)
