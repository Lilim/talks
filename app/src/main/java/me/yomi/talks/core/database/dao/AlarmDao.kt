package me.yomi.talks.core.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import me.yomi.talks.core.database.entities.AlarmEntity
import me.yomi.talks.core.database.entities.ColorEntity

@Dao
interface AlarmDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(guid: AlarmEntity): Long

    @Query("SELECT * FROM alarm")
    suspend fun getAlarms(): List<AlarmEntity>

    @Query("SELECT * FROM alarm WHERE request_code = :requestCode")
    suspend fun getAlarmByRequestCode(requestCode: Int): List<AlarmEntity>

    @Query("DELETE FROM alarm WHERE guid = :guid")
    suspend fun deleteAlarm(guid: String)
}