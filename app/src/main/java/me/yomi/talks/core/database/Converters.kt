package me.yomi.talks.core.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import me.yomi.talks.core.network.models.Persons

class Converters {

    @TypeConverter
    fun toArray(string: String): Array<Persons> {
        val gson = Gson()
        val listType = object : TypeToken<Array<Persons>>() {}.type
        return gson.fromJson(string, listType)
    }

    @TypeConverter
    fun fromArray(list: Array<Persons>): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}