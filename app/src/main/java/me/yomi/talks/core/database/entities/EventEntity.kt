package me.yomi.talks.core.database.entities

import androidx.room.*
import me.yomi.talks.core.database.Converters
import me.yomi.talks.core.network.models.Persons

@Entity(tableName = "event")
class EventEntity (
    @PrimaryKey @ColumnInfo(name = "guid") var guid: String,
    @ColumnInfo(name = "id") var id: Long,
    @ColumnInfo(name = "date") var date: String,
    @ColumnInfo(name = "start") var start: String,
    @ColumnInfo(name = "duration") var duration: String,
    @ColumnInfo(name = "room") var room: String,
    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "subtitle") var subtitle: String,
    @ColumnInfo(name = "track") var track: String,
    @ColumnInfo(name = "type") var type: String,
    @ColumnInfo(name = "language") var language: String,
    @ColumnInfo(name = "abstract") var abstractString: String,
    @ColumnInfo(name = "description") var description: String,
    @ColumnInfo(name = "url") var url: String,
    @TypeConverters(Converters::class) @ColumnInfo(name = "persons") var persons: Array<Persons>?,
    @ColumnInfo(name = "day") var day: String,
    @ColumnInfo(name = "timestamp_start") var timestampStart: Long,
    @ColumnInfo(name = "timestamp_end") var timestampEnd: Long,
    @ColumnInfo(name = "searchable") var searchable: String,
    var isFavorite: Boolean = false
)