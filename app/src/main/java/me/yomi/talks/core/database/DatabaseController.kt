package me.yomi.talks.core.database

import android.content.ContentValues
import android.content.Context
import me.yomi.talks.core.network.models.EventItem
import me.yomi.talks.core.network.models.EventItemSchedule
import java.sql.SQLException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DatabaseController(private val context: Context) {
    var helper = DatabaseHelper(context)
    var db = helper.writableDatabase

    fun saveEvent(event: EventItem) {
        val savedEvent = DatabaseController(context).getEventByGuid(event.guid)

        if (savedEvent != null) {
            db.beginTransaction()
            val id = try {

                val values = ContentValues()
                values.put(DatabaseHelper.DatabaseValues.ID, event.id)
                values.put(DatabaseHelper.DatabaseValues.GUID, event.guid)
                values.put(DatabaseHelper.DatabaseValues.DATE, event.date)
                values.put(DatabaseHelper.DatabaseValues.START, event.start)
                values.put(DatabaseHelper.DatabaseValues.DURATION, event.duration)
                values.put(DatabaseHelper.DatabaseValues.ROOM, event.room)
                values.put(DatabaseHelper.DatabaseValues.TITLE, event.title)
                values.put(DatabaseHelper.DatabaseValues.SUBTITLE, event.subtitle)
                values.put(DatabaseHelper.DatabaseValues.TRACK, event.track)
                values.put(DatabaseHelper.DatabaseValues.TYPE, event.type)
                values.put(DatabaseHelper.DatabaseValues.LANGUAGE, event.language)
                values.put(DatabaseHelper.DatabaseValues.ABSTRACT, event.abstract)
                values.put(DatabaseHelper.DatabaseValues.DESCRIPTION, event.description)
                values.put(DatabaseHelper.DatabaseValues.URL, event.url)
                values.put(DatabaseHelper.DatabaseValues.SPEAKERS, event.speaker)

                val searchable =
                        "${event.room} ${event.title} ${event.subtitle} ${event.track} ${event.abstract} ${event.speaker}"
                values.put(DatabaseHelper.DatabaseValues.SEARCHABLE, searchable)

                val datetime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(event.date)
                values.put(DatabaseHelper.DatabaseValues.TIMESTAMP, datetime.time)

                val formatter = SimpleDateFormat("yyyy-MM-dd")
                val formattedDate = formatter.format(datetime.time)
                values.put(DatabaseHelper.DatabaseValues.DAY, formattedDate)

                val selectionArs = arrayOf(savedEvent.id.toString())
                val id = db.update(
                        DatabaseHelper.DatabaseValues.TABLE_EVENT,
                        values,
                        "${DatabaseHelper.DatabaseValues._ID}=?",
                        selectionArs
                )

                db.setTransactionSuccessful()
                id
            } catch (exception: SQLException) {

            } finally {
                db.endTransaction()
                db.close()
            }

        } else {
            DatabaseController(context).saveNewEvent(event)
        }
    }

    private fun saveNewEvent(event: EventItem) {
        val newEvent = ContentValues()

        newEvent.put(DatabaseHelper.DatabaseValues.ID, event.id)
        newEvent.put(DatabaseHelper.DatabaseValues.GUID, event.guid)
        newEvent.put(DatabaseHelper.DatabaseValues.DATE, event.date)
        newEvent.put(DatabaseHelper.DatabaseValues.START, event.start)
        newEvent.put(DatabaseHelper.DatabaseValues.DURATION, event.duration)
        newEvent.put(DatabaseHelper.DatabaseValues.ROOM, event.room)
        newEvent.put(DatabaseHelper.DatabaseValues.TITLE, event.title)
        newEvent.put(DatabaseHelper.DatabaseValues.SUBTITLE, event.subtitle)
        newEvent.put(DatabaseHelper.DatabaseValues.TRACK, event.track)
        newEvent.put(DatabaseHelper.DatabaseValues.TYPE, event.type)
        newEvent.put(DatabaseHelper.DatabaseValues.LANGUAGE, event.language)
        newEvent.put(DatabaseHelper.DatabaseValues.ABSTRACT, event.abstract)
        newEvent.put(DatabaseHelper.DatabaseValues.DESCRIPTION, event.description)
        newEvent.put(DatabaseHelper.DatabaseValues.URL, event.url)
        newEvent.put(DatabaseHelper.DatabaseValues.SPEAKERS, event.speaker)

        val searchable =
                "${event.room} ${event.title} ${event.subtitle} ${event.track} ${event.abstract} ${event.speaker}"
        newEvent.put(DatabaseHelper.DatabaseValues.SEARCHABLE, searchable)

        val datetime = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(event.date)
        newEvent.put(DatabaseHelper.DatabaseValues.TIMESTAMP, datetime.time)

        // Congress day is not a normal day
        // Find out if event starts befor 5 AM then put it to the event day before
        val calendar = Calendar.getInstance()
        calendar.time = datetime
        if (calendar.get(Calendar.HOUR_OF_DAY) < 5) {
            calendar.add(Calendar.DATE, -1)
        }
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        val formattedDate = formatter.format(calendar.time)

        newEvent.put(DatabaseHelper.DatabaseValues.DAY, formattedDate)

        db.beginTransaction()
        val id = try {
            val id = db.insert(DatabaseHelper.DatabaseValues.TABLE_EVENT, null, newEvent)
            db.setTransactionSuccessful()
            id
        } catch (exception: SQLException) {

        } finally {
            db.endTransaction()
            db.close()
        }
    }

    fun getEventByGuid(guid: String): EventItem? {
        val favoriteList = DatabaseController(context).getAllFavoritesGuids()

        val fields = arrayOf(
                DatabaseHelper.DatabaseValues._ID,
                DatabaseHelper.DatabaseValues.ID,
                DatabaseHelper.DatabaseValues.GUID,
                DatabaseHelper.DatabaseValues.DATE,
                DatabaseHelper.DatabaseValues.START,
                DatabaseHelper.DatabaseValues.DURATION,
                DatabaseHelper.DatabaseValues.ROOM,
                DatabaseHelper.DatabaseValues.TITLE,
                DatabaseHelper.DatabaseValues.SUBTITLE,
                DatabaseHelper.DatabaseValues.TRACK,
                DatabaseHelper.DatabaseValues.TYPE,
                DatabaseHelper.DatabaseValues.LANGUAGE,
                DatabaseHelper.DatabaseValues.ABSTRACT,
                DatabaseHelper.DatabaseValues.DESCRIPTION,
                DatabaseHelper.DatabaseValues.URL,
                DatabaseHelper.DatabaseValues.SPEAKERS,
                DatabaseHelper.DatabaseValues.TIMESTAMP,
                DatabaseHelper.DatabaseValues.DAY
        )

        val selection = "${DatabaseHelper.DatabaseValues.GUID}=?"
        val rowSelArg = arrayOf(guid)

        val cursor = db.query(
                DatabaseHelper.DatabaseValues.TABLE_EVENT,
                fields,
                selection,
                rowSelArg,
                null,
                null,
                null
        )

        while (cursor.moveToNext()) {
            val internalId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.DatabaseValues._ID))
            val id = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ID))
            val guid = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.GUID))
            val date = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DATE))
            val start = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.START))
            val duration =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DURATION))
            val room = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ROOM))
            val title = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TITLE))
            val subtitle =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.SUBTITLE))
            val track = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TRACK))
            val type = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TYPE))
            val language =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.LANGUAGE))
            val abstract =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ABSTRACT))
            val description =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DESCRIPTION))
            val url = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.URL))
            val speakers =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.SPEAKERS))
            val timestamp =
                    cursor.getLong(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TIMESTAMP))
            val day = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DAY))

            // Is Event a favorite?
            var favorite = false
            if (favoriteList.contains(guid)) {
                println("## true " + guid)
                favorite = true
            }

            println("## event " + title)

            val event = EventItem(
                    internalId,
                    id,
                    guid,
                    date,
                    start,
                    duration,
                    room,
                    title,
                    subtitle,
                    track,
                    type,
                    language,
                    abstract,
                    description,
                    url,
                    speakers,
                    favorite,
                    timestamp,
                    day
            )

            cursor.close()
            db.close()
            return event
        }

        cursor.close()
        db.close()

        return null
    }

    fun getAllEvents(): List<EventItem> {
        val list = arrayListOf<EventItem>()
        val favoriteList = DatabaseController(context).getAllFavoritesGuids()

        val fields = arrayOf(
                DatabaseHelper.DatabaseValues._ID,
                DatabaseHelper.DatabaseValues.ID,
                DatabaseHelper.DatabaseValues.GUID,
                DatabaseHelper.DatabaseValues.DATE,
                DatabaseHelper.DatabaseValues.START,
                DatabaseHelper.DatabaseValues.DURATION,
                DatabaseHelper.DatabaseValues.ROOM,
                DatabaseHelper.DatabaseValues.TITLE,
                DatabaseHelper.DatabaseValues.SUBTITLE,
                DatabaseHelper.DatabaseValues.TRACK,
                DatabaseHelper.DatabaseValues.TYPE,
                DatabaseHelper.DatabaseValues.LANGUAGE,
                DatabaseHelper.DatabaseValues.ABSTRACT,
                DatabaseHelper.DatabaseValues.DESCRIPTION,
                DatabaseHelper.DatabaseValues.URL,
                DatabaseHelper.DatabaseValues.SPEAKERS,
                DatabaseHelper.DatabaseValues.TIMESTAMP,
                DatabaseHelper.DatabaseValues.DAY
        )

        val cursor = db.query(
                DatabaseHelper.DatabaseValues.TABLE_EVENT,
                fields,
                null,
                null,
                null,
                null,
                null
        )

        while (cursor.moveToNext()) {
            val internalId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.DatabaseValues._ID))
            val id = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ID))
            val guid = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.GUID))
            val date = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DATE))
            val start = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.START))
            val duration =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DURATION))
            val room = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ROOM))
            val title = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TITLE))
            val subtitle =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.SUBTITLE))
            val track = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TRACK))
            val type = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TYPE))
            val language =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.LANGUAGE))
            val abstract =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ABSTRACT))
            val description =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DESCRIPTION))
            val url = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.URL))
            val speakers =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.SPEAKERS))
            val timestamp =
                    cursor.getLong(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TIMESTAMP))
            val day = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DAY))

            // Is Event a favorite?
            var favorite = false
            if (favoriteList.contains(guid)) {
                favorite = true
            }

            list.add(
                    EventItem(
                            internalId,
                            id,
                            guid,
                            date,
                            start,
                            duration,
                            room,
                            title,
                            subtitle,
                            track,
                            type,
                            language,
                            abstract,
                            description,
                            url,
                            speakers,
                            favorite,
                            timestamp,
                            day
                    )
            )
        }

        cursor.close()
        db.close()

        return list
    }

    fun getFavoriteByGuid(guid: String): Int {
        var id = 0

        val fields = arrayOf(
                DatabaseHelper.DatabaseValues._ID,
                DatabaseHelper.DatabaseValues.GUID
        )

        val selection = "${DatabaseHelper.DatabaseValues.GUID}=?"
        val rowSelArg = arrayOf(guid)

        val cursor = db.query(
                DatabaseHelper.DatabaseValues.TABLE_FAVORITE,
                fields,
                selection,
                rowSelArg,
                null,
                null,
                null
        )

        while (cursor.moveToNext()) {
            id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.DatabaseValues._ID))
        }

        cursor.close()
        db.close()

        return id
    }

    fun changeEventFavorite(guid: String) {
        val favoriteId = DatabaseController(context).getFavoriteByGuid(guid)

        if (favoriteId > 0) {
            db.beginTransaction()
            val id = try {
                val _success = db.delete(
                        DatabaseHelper.DatabaseValues.TABLE_FAVORITE,
                        DatabaseHelper.DatabaseValues._ID + "=?",
                        arrayOf(favoriteId.toString())
                ).toLong()
                db.setTransactionSuccessful()

            } catch (exception: SQLException) {
            } finally {
                db.endTransaction()
                db.close()
                println("## favoriteId Deleted ${favoriteId}")
            }
        } else {
            // save favorite
            val newFavorite = ContentValues()
            newFavorite.put(DatabaseHelper.DatabaseValues.GUID, guid)

            db.beginTransaction()
            val id = try {
                val id = db.insert(DatabaseHelper.DatabaseValues.TABLE_FAVORITE, null, newFavorite)
                db.setTransactionSuccessful()
                id
            } catch (exception: SQLException) {

            } finally {
                db.endTransaction()
                db.close()
                println("## favoriteId inserted ${favoriteId}")
            }
        }
    }

    fun getAllEventsByRoomAndTime(room: String, date: String): List<EventItem> {
        val list = arrayListOf<EventItem>()
        val favoriteList = DatabaseController(context).getAllFavoritesGuids()

        val fields = arrayOf(
                DatabaseHelper.DatabaseValues._ID,
                DatabaseHelper.DatabaseValues.ID,
                DatabaseHelper.DatabaseValues.GUID,
                DatabaseHelper.DatabaseValues.DATE,
                DatabaseHelper.DatabaseValues.START,
                DatabaseHelper.DatabaseValues.DURATION,
                DatabaseHelper.DatabaseValues.ROOM,
                DatabaseHelper.DatabaseValues.TITLE,
                DatabaseHelper.DatabaseValues.SUBTITLE,
                DatabaseHelper.DatabaseValues.TRACK,
                DatabaseHelper.DatabaseValues.TYPE,
                DatabaseHelper.DatabaseValues.LANGUAGE,
                DatabaseHelper.DatabaseValues.ABSTRACT,
                DatabaseHelper.DatabaseValues.DESCRIPTION,
                DatabaseHelper.DatabaseValues.URL,
                DatabaseHelper.DatabaseValues.SPEAKERS,
                DatabaseHelper.DatabaseValues.TIMESTAMP,
                DatabaseHelper.DatabaseValues.DAY
        )

        val selection =
                "${DatabaseHelper.DatabaseValues.ROOM}=? AND ${DatabaseHelper.DatabaseValues.DAY}=?"
        val rowSelArg = arrayOf(room, date)
        val order = DatabaseHelper.DatabaseValues.TIMESTAMP

        val cursor = db.query(
                DatabaseHelper.DatabaseValues.TABLE_EVENT,
                fields,
                selection,
                rowSelArg,
                null,
                null,
                order
        )

        println("## cursor rooms " + room + " " + cursor.count)

        while (cursor.moveToNext()) {
            val internalId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.DatabaseValues._ID))
            val id = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ID))
            val guid = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.GUID))
            val date = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DATE))
            val start = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.START))
            val duration =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DURATION))
            val room = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ROOM))
            val title = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TITLE))
            val subtitle =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.SUBTITLE))
            val track = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TRACK))
            val type = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TYPE))
            val language =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.LANGUAGE))
            val abstract =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ABSTRACT))
            val description =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DESCRIPTION))
            val url = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.URL))
            val speakers =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.SPEAKERS))
            val timestamp =
                    cursor.getLong(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TIMESTAMP))
            val day = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DAY))

            // Is Event a favorite?
            var favorite = false
            if (favoriteList.contains(guid)) {
                println("## event is fav ")
                favorite = true
            }

            list.add(
                    EventItem(
                            internalId,
                            id,
                            guid,
                            date,
                            start,
                            duration,
                            room,
                            title,
                            subtitle,
                            track,
                            type,
                            language,
                            abstract,
                            description,
                            url,
                            speakers,
                            favorite,
                            timestamp,
                            day
                    )
            )

//            println("## " + title + " fav " +favorite)
        }

        cursor.close()
        db.close()

        return list
    }

    fun getAllFavoritesGuids(): List<String> {
        val list = arrayListOf<String>()

        val fields = arrayOf(
                DatabaseHelper.DatabaseValues._ID,
                DatabaseHelper.DatabaseValues.GUID
        )

        val cursor = db.query(
                DatabaseHelper.DatabaseValues.TABLE_FAVORITE,
                fields,
                null,
                null,
                null,
                null,
                null
        )

        println("## getAllFavoritesGuids " + cursor.count)

        while (cursor.moveToNext()) {
            val guid = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.GUID))

            if (guid != null) {
                list.add(guid)
            }
        }

        cursor.close()
        db.close()

        return list
    }

    fun getAllFavorites(): List<EventItem> {
        val list = arrayListOf<EventItem>()

        val fields = arrayOf(
                DatabaseHelper.DatabaseValues._ID,
                DatabaseHelper.DatabaseValues.GUID
        )

        val cursor = db.query(
                DatabaseHelper.DatabaseValues.TABLE_FAVORITE,
                fields,
                null,
                null,
                null,
                null,
                null
        )

        println("## getAllFavorites " + cursor.count)

        while (cursor.moveToNext()) {
            val guid = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.GUID))

            val eventData = DatabaseController(context).getEventByGuid(guid)
            if (eventData != null) {
                list.add(eventData)
            }
        }

        if (list.size > 0) {
            return list.sortedWith(compareBy({ it.timestamp }))
        }

        cursor.close()
        db.close()

        return list
    }

    fun getFavoritesForScheduleByDate(day: String): List<EventItemSchedule> {
        val list = arrayListOf<EventItemSchedule>()

        val sql =
                "SELECT ${DatabaseHelper.DatabaseValues.TABLE_FAVORITE}.${DatabaseHelper.DatabaseValues.GUID} FROM ${DatabaseHelper.DatabaseValues.TABLE_FAVORITE} LEFT JOIN ${DatabaseHelper.DatabaseValues.TABLE_EVENT} " +
                        " ON ${DatabaseHelper.DatabaseValues.TABLE_FAVORITE}.${DatabaseHelper.DatabaseValues.GUID} = ${DatabaseHelper.DatabaseValues.TABLE_EVENT}.${DatabaseHelper.DatabaseValues.GUID} " +
                        " WHERE ${DatabaseHelper.DatabaseValues.TABLE_EVENT}.${DatabaseHelper.DatabaseValues.DAY} = '${day}'; "

        val cursor = db.rawQuery(sql, null)

        println("## sql " + sql)
        println("## getAllFavorites " + cursor.count)

        while (cursor.moveToNext()) {
            val guid = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.GUID))

            val eventData = DatabaseController(context).getEventByGuid(guid)
//            if (eventData != null) {
//                list.add(EventItemSchedule(eventData))
//            }
        }

        if (list.size > 0) {
//            return list.sortedWith(compareBy({ it.timestamp }))
        }

        cursor.close()
        db.close()

        return list
    }

    fun getAllFavoritesForSchedule(): List<EventItemSchedule> {
        val list = arrayListOf<EventItemSchedule>()

        val fields = arrayOf(
                DatabaseHelper.DatabaseValues._ID,
                DatabaseHelper.DatabaseValues.GUID
        )

        val cursor = db.query(
                DatabaseHelper.DatabaseValues.TABLE_FAVORITE,
                fields,
                null,
                null,
                null,
                null,
                null
        )

        println("## getAllFavorites " + cursor.count)

        while (cursor.moveToNext()) {
            val guid = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.GUID))

            val eventData = DatabaseController(context).getEventByGuid(guid)
//            if (eventData != null) {
//                list.add(EventItemSchedule(eventData))
//            }
        }

        if (list.size > 0) {
//            return list.sortedWith(compareBy({ it.timestamp }))
        }

        cursor.close()
        db.close()

        return list
    }

    fun getRooms(): ArrayList<String> {
        val roomsList = ArrayList<String>()

        val sql =
                "SELECT DISTINCT ${DatabaseHelper.DatabaseValues.ROOM} FROM ${DatabaseHelper.DatabaseValues.TABLE_EVENT}"

        val cursor = db.rawQuery(sql, null)

        while (cursor.moveToNext()) {
            val room = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ROOM))
            roomsList.add(room)
        }
        cursor.close()
        db.close()

        return roomsList
    }

    fun getDays(): ArrayList<String> {
        val daysList = ArrayList<String>()

        val sql =
                "SELECT DISTINCT ${DatabaseHelper.DatabaseValues.DAY} FROM ${DatabaseHelper.DatabaseValues.TABLE_EVENT}"

        val cursor = db.rawQuery(sql, null)

        while (cursor.moveToNext()) {
            val day = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DAY))
            daysList.add(day)
        }
        cursor.close()
        db.close()

        return daysList
    }

    fun getDataBySearch(query: String): ArrayList<EventItem> {
        val list = ArrayList<EventItem>()
        val favoriteList = DatabaseController(context).getAllFavoritesGuids()

        val sql =
                "SELECT * FROM ${DatabaseHelper.DatabaseValues.TABLE_EVENT} WHERE ${DatabaseHelper.DatabaseValues.SEARCHABLE} LIKE '%${query}%'"

        val cursor = db.rawQuery(sql, null)

        println("## cursor search " + query + " " + cursor.count)

        while (cursor.moveToNext()) {
            val internalId = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.DatabaseValues._ID))
            val id = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ID))
            val guid = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.GUID))
            val date = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DATE))
            val start = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.START))
            val duration =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DURATION))
            val room = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ROOM))
            val title = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TITLE))
            val subtitle =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.SUBTITLE))
            val track = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TRACK))
            val type = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TYPE))
            val language =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.LANGUAGE))
            val abstract =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.ABSTRACT))
            val description =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DESCRIPTION))
            val url = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.URL))
            val speakers =
                    cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.SPEAKERS))
            val timestamp =
                    cursor.getLong(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.TIMESTAMP))
            val day = cursor.getString(cursor.getColumnIndex(DatabaseHelper.DatabaseValues.DAY))

            // Is Event a favorite?
            var favorite = false
            if (favoriteList.contains(guid)) {
                favorite = true
            }

            list.add(
                    EventItem(
                            internalId,
                            id,
                            guid,
                            date,
                            start,
                            duration,
                            room,
                            title,
                            subtitle,
                            track,
                            type,
                            language,
                            abstract,
                            description,
                            url,
                            speakers,
                            favorite,
                            timestamp,
                            day
                    )
            )

//            println("## " + title + " fav " +favorite)
        }

        cursor.close()
        db.close()

        return list
    }
}