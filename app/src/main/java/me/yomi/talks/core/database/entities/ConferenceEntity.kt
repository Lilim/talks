package me.yomi.talks.core.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "conference")
class ConferenceEntity (
    @PrimaryKey @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "version") val version: String,
    @ColumnInfo(name = "start") val start: String,
    @ColumnInfo(name = "end") val end: String,
    @ColumnInfo(name = "days_count") val daysCount: Int
)