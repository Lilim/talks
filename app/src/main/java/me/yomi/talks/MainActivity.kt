package me.yomi.talks

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import me.yomi.talks.core.Constants
import me.yomi.talks.core.network.ResponseStatus
import me.yomi.talks.core.prefs
import me.yomi.talks.databinding.ActivityMainBinding
import me.yomi.talks.ui.ActivityWithDetails
import me.yomi.talks.ui.UpdateDataListener
import me.yomi.talks.ui.custom.SpinnerSelect
import me.yomi.talks.ui.detail.DetailActivity
import me.yomi.talks.ui.home.HomeFragment
import me.yomi.talks.ui.search.SearchActivity
import me.yomi.talks.ui.settings.SettingsFragment
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : ActivityWithDetails(), UpdateDataListener {
    private lateinit var model: MainViewModel
    private lateinit var binding: ActivityMainBinding
    private lateinit var homeFragment: HomeFragment
    private lateinit var snackbar: Snackbar
    private val DETAILS_OPENED = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model =
            ViewModelProvider(this, ViewModelFactory(application)).get(MainViewModel::class.java)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        title = getString(R.string.name_36C3)
        setSupportActionBar(binding.myToolbar)

        if (prefs.sharedPrefs.getBoolean(Constants.FIRSTRUN, true)) {
            model.initRoomSort()
            prefs.sharedPrefs.edit().putBoolean(Constants.FIRSTRUN, false).apply()
        }

        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val formattedDate = formatter.format(Date(System.currentTimeMillis()))
        model.today = formattedDate

        if (intent.extras != null) {
            var guid = ""
            if (intent.extras != null) {
                guid = intent.getStringExtra("guid").orEmpty()
            }
            if (guid.isNotEmpty()) {
                val intent = Intent(this, DetailActivity::class.java)
                intent.putExtra("guid", guid)
                startActivityForResult(intent, DETAILS_OPENED)
            }
        }

        setObserver()
        setClickListener()
        createSnackbar()

        homeFragment = HomeFragment()
        addFragment(homeFragment, "HOME")
    }

    override fun onStart() {
        super.onStart()
        model.getDays()

        // Auto check every 10 minutes for new version
        val now = System.currentTimeMillis()
        if ((prefs.sharedPrefs.getLong(Constants.UPDATE, 0) + (3600 * 10)) < now) {
            model.fetchData()
        }
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 1) {
            val fragment = supportFragmentManager.findFragmentById(R.id.fragmentContainer)
            if (fragment is SettingsFragment) {
                if (fragment.checkForChanges()) {
                    prefs.updateSettingsPrefs()
                    updateViewPager(model.currentDate)
                }
            }
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            supportFragmentManager.popBackStack()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setClickListener() {
        binding.openSearch.setOnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivityForResult(intent, Constants.SEARCHCLOSED)
        }

        binding.update.setOnClickListener {
            model.fetchData()
        }

        binding.settings.setOnClickListener {
            openSettings()
        }
    }

    private fun setObserver() {
        model.daysList.observe(this, Observer {
            updateDateSpinner(it)
            if (it.isNotEmpty()) {
                updateViewPager(it[0])
            }
        })

        model.updating.observe(this, Observer {
            if (it) {
                snackbar.show()
            } else {
                snackbar.dismiss()
            }
        })

        model.error.observe(this, Observer {
            if (it) {
                Toast.makeText(
                    this,
                    "An error has occured while fetching data from server.\nPlease try again later.",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }

    private fun addFragment(fragment: Fragment, tag: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragmentContainer, fragment)
        transaction.addToBackStack(tag)
        transaction.commit()
    }

    private fun openSettings() {
        val settingFragment = SettingsFragment()
        addFragment(settingFragment, "SETTINGS")
    }

    private fun updateDateSpinner(dateList: List<String>) {
        val adapter = ArrayAdapter(this, R.layout.spinner_view, dateList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinner.adapter = adapter
        binding.spinner.onItemSelectedListener = SpinnerSelect(this, dateList)
        // Preselect current date
        if (model.today.isNotEmpty()) {
            val pos = dateList.indexOf(model.today)
            binding.spinner.setSelection(pos)
        }
    }

    fun updateViewPager(date: String) {
        model.currentDate = date
        homeFragment.updateData(date, false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == DETAILS_OPENED) {
            if (resultCode == Activity.RESULT_OK) {
                updateViewPager(model.currentDate)
            }
        }
    }

    private fun createSnackbar() {
        snackbar = Snackbar.make(
            binding.fragmentContainer,
            "Updating ....",
            Snackbar.LENGTH_INDEFINITE
        ).setAction("Action", null)
        snackbar.setActionTextColor(Color.WHITE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundResource(R.drawable.gradient_35c3)
        val textView =
            snackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.WHITE)
        textView.textSize = 18f
    }

    override fun dataFetched(responseStatus: ResponseStatus) {
        when (responseStatus) {
            ResponseStatus.Success -> {
                model.getDays()
            }
            else -> {
            }
        }
    }

    override fun onBackButtonPressed() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun openedSubview(name: String) {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = name
        val spinner = findViewById<Spinner>(R.id.spinner)
        spinner.visibility = View.GONE
        binding.settings.visibility = View.GONE
        binding.openSearch.visibility = View.GONE
        binding.update.visibility = View.GONE
    }

    override fun closedSubview() {
        supportActionBar?.setDisplayHomeAsUpEnabled(false)
        title = getString(R.string.name_36C3)
        val spinner = findViewById<Spinner>(R.id.spinner)
        spinner.visibility = View.VISIBLE
        binding.settings.visibility = View.VISIBLE
        binding.openSearch.visibility = View.VISIBLE
        binding.update.visibility = View.VISIBLE
    }
}
